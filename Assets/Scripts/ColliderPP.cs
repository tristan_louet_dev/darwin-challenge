﻿using UnityEngine;
using System.Collections;

public class ColliderPP : MonoBehaviour {

    public Vector3 ReturnDirection;

	// Use this for initialization
	void Start () {
	
	}


    void OnTriggerStay2D(Collider2D other)
    {
        Rigidbody2D rigidbody = other.gameObject.GetComponent<Rigidbody2D>();
        if (rigidbody != null)
        {
            rigidbody.velocity = Vector2.zero;
        }
        other.gameObject.transform.position = Vector2.zero;
	}



}
