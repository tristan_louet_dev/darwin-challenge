﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
 
public class Hat : MonoBehaviour {

    public Collider2D Feet;
    public GameObject Player;

    private List<GameObject> _platforms;

	// Use this for initialization
	void Start () {
        _platforms = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {
        if (_platforms.Count > 0 && Player.GetComponent<Rigidbody2D>().velocity.y > 0)
        {
            Feet.isTrigger = true;
        }
        //if (_platforms.Count <= 0)
        //{
        //    Feet.isTrigger = false;
        //}
	}

    void OnTriggerStay2D(Collider2D other)
    {
        if (!_platforms.Contains(other.gameObject))
        {
            _platforms.Add(other.gameObject);
        }
    }

    //void OnTriggerEnter2D(Collider2D other)
    //{
    //    if (!_platforms.Contains(other.gameObject))
    //    {
    //        _platforms.Add(other.gameObject);
    //    }
    //}

    void OnTriggerExit2D(Collider2D other)
    {
        if (_platforms.Contains(other.gameObject))
        {
            _platforms.Remove(other.gameObject);
        }
    }

}
