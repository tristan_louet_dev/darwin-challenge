﻿using Assets.Scripts.Entities;
using UnityEngine;

public class ClickToImpulse : MonoBehaviour
{
    [Range(1f, 1000)] [SerializeField] private float _force = 5f;
    [SerializeField] private ForceMode2D _forceMode = ForceMode2D.Impulse;

	// Update is called once per frame
	void Update () {
	    if (Input.GetMouseButtonDown(0))
	    {
	        var impact = Camera.main.ScreenToWorldPoint(Input.mousePosition);
	        var ball = FindObjectOfType<Ball>();
	        ball.GetComponent<Rigidbody2D>().AddForceAtPosition((ball.transform.position - impact) * _force, impact, _forceMode);
	    }
	}
}
