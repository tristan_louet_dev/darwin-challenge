﻿using System.CodeDom;
using Assets.Scripts.Managers.Events;
﻿using Assets.Scripts.Managers;
using Assets.Scripts.Utils;
using Audio;
using UnityEngine;

namespace Assets.Scripts.Entities
{
    [RequireComponent(typeof(Rigidbody2D), typeof(Collider2D), typeof(TrailRenderer))]
    public class Ball : MonoBehaviour {
        #region enums
        public enum State
        {
            OwnedByPlayer,
            Normal,
            Powerful,
            UberPowerful,
        }

        public enum Type
        {
            Normal,
            Bouncy,
            Squared,
        }
        #endregion

        private TrailRenderer _trail;
        private Rigidbody2D _rigidbody;
        private Vector2 _velocity;
        
        [SerializeField] private  Color _normalColor = new Color(1f, 1f, 1f, 0.3f);
        [SerializeField] private float _powefulStateThreshold = 100f;
        [SerializeField] private Color _powerfulColor = new Color(1f, 0.686f, 0.137f, 0.3f);
        [SerializeField] private float _uberPowerfulStateThreshold = 300f;
        [SerializeField] private Color _uberPowerfulColor = new Color(1f, 0f, 0f, 0.3f);
        [SerializeField] private HitResolverBall _hitResolverBall;

        [SerializeField] private Type _type = Type.Normal;

        [SerializeField]
        private float speed = 0.0f;

        public State CurrentState
        {
            get
            {
                if (Owner != null)
                {
                    return State.OwnedByPlayer;
                }

                if(_rigidbody.velocity.SqrMagnitude() > _uberPowerfulStateThreshold)
                {
                    return State.UberPowerful;
                }

                if (_rigidbody.velocity.SqrMagnitude() > _powefulStateThreshold)
                {
                    return State.Powerful;
                }

                return State.Normal;

                /*
                if (_rigidbody.velocity.Any(n => n > _uberPowerfulStateThreshold))
                {
                    return State.UberPowerful;
                }
                if (_rigidbody.velocity.Any(n => n > _powefulStateThreshold))
                {
                    return State.Powerful;
                }
                return State.Normal; */
            }
        }

        public Team LastTeamToHit { get; private set; }

        public Event<Player, Player> OnOwnerChange { get; set; }
        public Player LastOwner { get; private set; }
        private float _timeAtLastRelease;
        [SerializeField] private float _timeoutBetweenOwnerChange = 0.05f;
        private float _timeAtLastSound;

        private Player _owner;

        public Player Owner
        {
            get { return _owner; }
            private set
            {
                LastOwner = _owner;
                _owner = value;
                _rigidbody.isKinematic = (value != null);
                GetComponent<Collider2D>().enabled = !_rigidbody.isKinematic;
                if (_rigidbody.isKinematic)
                {
                    _rigidbody.velocity = Vector2.zero;
                }
                _timeAtLastRelease = Time.time;
                OnOwnerChange.Fire(LastOwner, _owner);
            }
        }

        public void HitBy(Player p, Vector2 direction, float forcePerso)
        {
            //Owner.OwnedBall = null;
            Owner = null;
            var d = direction.normalized;
            transform.position = (Vector2) p.gameObject.transform.position + d*0.5F; //euh.. pourquoi on tp la ball ?
            //_rigidbody.AddForceAtPosition(direction.normalized*forcePerso, (Vector2)transform.position/*-(Vector2)direction.normalized*/, ForceMode2D.Impulse);
			_rigidbody.AddForce(d*forcePerso, ForceMode2D.Impulse);

            if (p != null)
            {
                LastTeamToHit = p.Team;
            }
        }

        public void Hit( Vector2 direction, float forcePerso)
        {
            //Owner.OwnedBall = null;
            Owner = null;
            var d = direction.normalized;
            //transform.position = (Vector2) g.transform.position + d*0.5F; //euh.. pourquoi on tp la ball ?
            //_rigidbody.AddForceAtPosition(direction.normalized*forcePerso, (Vector2)transform.position/*-(Vector2)direction.normalized*/, ForceMode2D.Impulse);
            _rigidbody.AddForce(d * forcePerso, ForceMode2D.Impulse);
        }

        /// <summary>
        /// Try to own the ball to the Player.
        /// </summary>
        /// <param name="p">The Player trying to own the Ball.</param>
        /// <returns>Returns true if the player is now the owner, false otherwise.</returns>
        public bool TryToOwn(Player p)
        {
            if (Owner != null)
            {
                return false;
            }
            if ((LastOwner == p) && (Time.time - _timeAtLastRelease < _timeoutBetweenOwnerChange))
            {
                return false;
            }
            Owner = p;
            return true;
        }

        void Awake()
        {
            _hitResolverBall = GetComponent<HitResolverBall>();
            _trail = GetComponent<TrailRenderer>();
            _trail.sortingOrder = 0;
            _rigidbody = GetComponent<Rigidbody2D>();
            OnOwnerChange = new Event<Player, Player>();
            Owner = null;
            BallsManager.Instance.Balls.Add(this);
            _timeAtLastSound = Time.time;
        }

        void Update()
        {
            if (Owner != null)
            {
                transform.position = Vector2.SmoothDamp(transform.position, Owner.transform.position, ref _velocity, 0.05f);
            }
            var c = _normalColor;
            if (CurrentState == State.Powerful)
            {
                c = _powerfulColor;
            } 
            else if (CurrentState == State.UberPowerful)
            {
                c = _uberPowerfulColor;
            }
            _trail.material.SetColor("_TintColor", c);

            speed = _rigidbody.velocity.SqrMagnitude();
        }

        void OnDestroy()
        {
            BallsManager.Instance.Balls.Remove(this);
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            var ad = GetComponent<AudioData>();
            if (Time.time - _timeAtLastSound > 0.2f)
            {
                if (ad.m_clips.Count > 0)
                {
                    AudioManager.Instance.Play(ad);
                }
            }
            _timeAtLastSound = Time.time;
        }

        public void RemoveOwner()
        {
            Owner = null;
        }

    }
}
