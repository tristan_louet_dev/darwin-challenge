﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Managers.Events;
using UnityEngine;
using Audio;

namespace Assets.Scripts.Entities.Spawners
{
    public class BallsSpawner : MonoBehaviour
    {
        [SerializeField] private Vector2 _initialPushDirection;
        [SerializeField] [Range(0.1f, 1000f)] private float _initialPushForce = 1.0f;

        private Dictionary<Ball.Type, GameObject> _typePrefab;

        private AudioData _spawnSound;

        void Awake()
        {
            _spawnSound = GetComponent<AudioData>();
            _typePrefab = new Dictionary<Ball.Type, GameObject>
            {
                {Ball.Type.Normal, Resources.Load<GameObject>("NormalBall")},
                {Ball.Type.Bouncy, Resources.Load<GameObject>("BouncyBall")},   
                {Ball.Type.Squared, Resources.Load<GameObject>("SquaredBall")},
            };
            _initialPushDirection.Normalize();
            BallsSpawnerManager.Instance.Register(this);
        }

        public void Spawn(Ball.Type type, float secondsBeforeSpawn = 0f, Action<Ball> callback = null )
        {
            StartCoroutine(TimedSpawn(type, secondsBeforeSpawn, callback));
        }

        private IEnumerator TimedSpawn(Ball.Type type, float secondsBeforeSpawn, Action<Ball> callback)
        {
            yield return new WaitForSeconds(secondsBeforeSpawn);
            var b = Instantiate(_typePrefab[type]).GetComponent<Ball>();
            b.transform.position = transform.position;
            b.Hit(_initialPushDirection, _initialPushForce);
            EventManager.OnNewBallSpawned.Fire(b, this);
            AudioManager.Instance.Play(_spawnSound);
            if (callback != null)
            {
                callback(b);
            }
        }
    }
}
