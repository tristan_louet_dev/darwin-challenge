﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Managers;
using Assets.Scripts.Managers.Events;
using UnityEngine;

namespace Assets.Scripts.Entities.Spawners
{
    public class BallsSpawnerManager : Singleton<BallsSpawnerManager>
    {
        [SerializeField]
        private List<GameObject> ballSpawners = null;

        private List<BallsSpawner> _spawners;

        public float respawnTime = 3f;

        public override void Awake()
        {
            print("AWAKEN BALL SPAWN MANAGER");
            base.Awake();
            _spawners = new List<BallsSpawner>();
            EventManager.OnGameStarting += () =>
            {
                _spawners[Random.Range(0, _spawners.Count)].Spawn((Ball.Type)Random.Range(0, 3), 2.5f);
            };

            StartCoroutine(CountNextBall(4f));

            EventManager.OnGoal += (p, b, g, t) =>
            {
                _spawners[Random.Range(0, _spawners.Count)].Spawn((Ball.Type)Random.Range(0, 3), respawnTime, (newBall) => Destroy(b.gameObject));
                StartCoroutine(CountNextBall(3.2f));  
            };

            EventManager.OnNextBallTimerTick += (t) => Debug.Log("Next ball in " + t + "...");
            EventManager.OnNewBallSpawned += (b, s) => Debug.Log("New ball spawned ! " + b + " by " + s);
        }

        private IEnumerator CountNextBall(float seconds)
        {
            var ceilInt = Mathf.CeilToInt(seconds);
            while (true)
            {
                yield return null;
                seconds -= Time.deltaTime;
                var oldInt = ceilInt;
                ceilInt = Mathf.CeilToInt(seconds);
                if (ceilInt == 0)
                {
                    yield break;
                }
                if (ceilInt < oldInt)
                {
                    EventManager.OnNextBallTimerTick.Fire(ceilInt);
                }
            }
        }
        
        public void Register(BallsSpawner s)
        {
            _spawners.Add(s);
            ballSpawners.Add(s.gameObject);
            Debug.Log(_spawners.Count + " spawners registered.");
        }

        public void ClearSpawners()
        {
            _spawners.Clear();
        }

        void OnDestroy()
        {
            EventManager.OnGameStarting -= () =>
            {
                _spawners[0].Spawn((Ball.Type)Random.Range(0, 3), 2.5f);
            };

            EventManager.OnGoal -= (p, b, g, t) =>
            {
                _spawners[0].Spawn((Ball.Type)Random.Range(0, 3), respawnTime, (newBall) => Destroy(b.gameObject));
                StartCoroutine(CountNextBall(3.2f));
            };


            ClearSpawners();
        }
    }
}
