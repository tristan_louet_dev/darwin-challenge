﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Entities
{
    public enum Team {
        None,
        Red,
        Blue,
    }

    public static class TeamExtensions
    {
        public static IEnumerable<Team> Values()
        {
            return (Team[]) Enum.GetValues(typeof (Team));
        }

        public static bool AreEnemies(Team t1, Team t2)
        {
            return (t1 == Team.Red) && (t2 == Team.Blue) || (t1 == Team.Blue) && (t2 == Team.Red);
        }

        public static bool IsEnemy(this Team t1, Team t2)
        {
            return AreEnemies(t1, t2);
        }

        public static IEnumerable<Team> OppositeTeams(this Team t)
        {
            return (from team in Values() where team != t && team != Team.None select team).ToList();
        }

        public static string Name(this Team t)
        {
            return t == Team.Red ? "yellow" : t.ToString().ToLower();
        }
    }
}