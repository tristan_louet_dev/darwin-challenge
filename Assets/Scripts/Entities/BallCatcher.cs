﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Entities;
namespace Assets.Scripts.Entities
{
    [RequireComponent(typeof(Collider2D))]
    public class BallCatcher : MonoBehaviour
    {

        public Player Player;
        public bool canCatch = true;

        // Use this for initialization
        void Start()
        {
            canCatch = true;
            if (Player == null)
                Player = transform.parent.gameObject.GetComponent<Player>();
        }

        // Update is called once per frame
        void Update()
        {
            transform.position = transform.parent.transform.position;
        }

        void OnTriggerStay2D(Collider2D other)
        {
            if (Player.OwnedBall == null && canCatch)
            {
                if (other.gameObject.tag == "Ball")
                {
                    var b = other.gameObject.GetComponent<Ball>();
                    if (b.TryToOwn(Player))
                    {
                        Player.SetBall(b);
                    }
                }
            }
        }

        void OnTriggerExit2D(Collider2D other)
        {
            if(other.gameObject.tag == "Ball")
            {
                Player.OwnedBall = null;
            }
        }
    }

}