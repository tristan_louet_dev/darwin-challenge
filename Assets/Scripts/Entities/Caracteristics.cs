﻿
namespace Assets.Scripts.Entities
{
    interface ICaracteristics
    {
        float ForceJumpCoef { get; }
        float MaxSpeedCoef { get;  }
        float GripRangeCoef { get;  }
        float InertiaCoef { get; }
        float ResistanceCoef { get; }
        float AirControlCoef { get;  }
        float AccelerationCoef { get; }
    }
}
