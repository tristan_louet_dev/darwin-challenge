﻿using UnityEngine;
using System.Collections;

public class GroundCheck_v2 : MonoBehaviour {

    private bool _groundCheck;

    public bool GroundCheck { get { return _groundCheck; } }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void FixedUpdate()
    {

    }


    void OnTriggerStay2D(Collider2D other)
    {
        //if ((other.gameObject.transform.position - transform.position).normalized. y)
        _groundCheck = true;
        if (other.gameObject.CompareTag("MovingPlatform")) transform.parent.transform.parent = other.transform;
	}

    void OnTriggerExit2D(Collider2D other)
    {
        _groundCheck = false;
        if (other.gameObject.CompareTag("MovingPlatform")) transform.parent.transform.parent = null;
    }

}

