﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Entities;

public class CaracteristicsBodyPart : MonoBehaviour , ICaracteristics
{
    [SerializeField]
    private float _forceJumpCoef;

    public float ForceJumpCoef
    {
        get { return _forceJumpCoef; }
        set { _forceJumpCoef = value; }
    }

    [SerializeField] private float _maxSpeedCoef;

    public float MaxSpeedCoef
    {
        get { return _maxSpeedCoef; }
        set { _maxSpeedCoef = value; }
    }

    [SerializeField] private float _gripRangeCoef;

    public float GripRangeCoef
    {
        get { return _gripRangeCoef; }
        set { _gripRangeCoef = value; }
    }


    [SerializeField] private float _inertiaCoef;

    public float InertiaCoef
    {
        get { return _inertiaCoef; }
        set { _inertiaCoef = value; }
    }

    [SerializeField] private float _resistanceCoef;

    public float ResistanceCoef
    {
        get { return _resistanceCoef; }
        set { _resistanceCoef = value; }
    }

    [SerializeField] private float _airControlCoef;

    public float AirControlCoef 
    {
        get { return _airControlCoef; }
        set { _airControlCoef = value; }
    }

    [SerializeField] private float _accelerationCoef;

    public float AccelerationCoef
    {
        get { return _accelerationCoef; }
        set { _accelerationCoef = value; }
    }
}
