﻿using Utils;

namespace Assets.Scripts.Entities
{
    public class HumanPLayer
    {
        private X360GamePad xpad { set; get; }
    
        public Player Player { private set; get; }
        // Use this for initialization

        public int Id;
        public HumanPLayer(X360GamePad xpad, Player p, int id)
        {
            this.Id = id;
            this.Player = p;
            this.Player.xpad = xpad;
            this.xpad = xpad;
        }
    }
}
