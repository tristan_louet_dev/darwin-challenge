﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts.Entities;
using Assets.Scripts.Managers;
using Audio;

[RequireComponent(typeof(CaracteristicsBodyPart), typeof(Collider2D), typeof(Animator))]
public abstract class BodyPart : MonoBehaviour
{
    public Sprite highSpriteYellow;
    public Sprite highSpriteBlue;

    private Collider2D _collider;
    protected Animator _animator;

    public bool _debugAnimation = false;
    public bool isTorso = false;

    public bool isAction=false;

    public CaracteristicsBodyPart CaracteristicsBodyPart { get; private set; }

    public Player Player;

    //TODO
    //public PlayerCracteristicsCoefs PlayerCracteristicsCoefs;

    public enum States { iddle, startAction, loopAction, endAction, jump, fall, run, hit };

    public GameObject Parent; //

    public bool CanHit; //can hit ?
    public float Force; //hit force
    public float ActiveDuration; //action duration which does something (like hit)
    public float BeginActionDuration; //action duration before it does something
    public float EndActionDuration; //action duration after it did something
    public float CoolddownDuration; // duration after action when he can't do something
    private float CoolDownBegin;
    public States state;
    public States oldState;
    public Vector2 HitDirectionVector;
    public float angleOffset;
    private Vector2 _lastDirectionVector;


    public GameObject hitBallSound;
    protected AudioData _hitBallSoundData;


    protected List<GameObject> _inRangeObjects;
    protected float _elaspedTimeInState = 0.0F;
    protected HitResolver _hitResolver;
    protected Dictionary<Player, bool> PlayerTriggered;
    protected Dictionary<Ball, bool> BallTriggered;

	// Use this for initialization
    protected virtual void Start()
    {
        CoolDownBegin = -CoolddownDuration;
        state = States.iddle;
        oldState = States.loopAction;
        _inRangeObjects = new List<GameObject>();
        CaracteristicsBodyPart = GetComponent<CaracteristicsBodyPart>();
        _collider = GetComponent<Collider2D>();
        _animator = GetComponent<Animator>();
        Player = transform.parent.gameObject.GetComponent<Player>();
        if (Parent == null)
        {
            Parent = transform.parent.gameObject;
        }
        _hitBallSoundData = hitBallSound.GetComponent<AudioData>();
    }

    protected string TeamDependantStateName(string stateName)
    {
        return stateName + "-" + Player.Team.Name();
    }

    protected void SwitchToAnimationState(string stateName, bool platformIndependant)
    {
        _animator.CrossFade(platformIndependant ? stateName : TeamDependantStateName(stateName), 0f);
    }

    protected virtual void Update()
    {
        #region debug
        if (_debugAnimation)
        {
            var states = new String[]{"idle", "idle-blue", "idle-yellow", "punch-blue", "punch-yellow"};
            var endstr = "unknown";

            foreach (var s in states.Where(s => _animator.GetCurrentAnimatorStateInfo(0).IsName("Base." + s)))
            {
                endstr = s;
                break;
            }
            Debug.Log("Animation state is " + endstr);
            _debugAnimation = false;
        }
        #endregion

        if (Player.xpad.LeftJoystickHorizontal.Value != 0) _lastDirectionVector = new Vector2(Player.xpad.LeftJoystickHorizontal.Value, 0).normalized;

        _elaspedTimeInState += Time.deltaTime;

        switch (state)
        {
            case States.iddle:
                if (oldState != States.iddle)
                {
                    print("switch to idle");
                    oldState = States.iddle;
                    SwitchToAnimationState("idle" + (isInCoolDown() ? "-cooldown" : ""), !isTorso);
                }    
                break;
            case States.startAction:
                if (_elaspedTimeInState >= BeginActionDuration && !isInCoolDown())
                {   
                    this.PrepareAction();
                    PlayerTriggered = new Dictionary<Player, bool>{{this.Player, true}}; 
                    BallTriggered = new Dictionary<Ball, bool>();
                    oldState = state;
                    state = States.loopAction;
                    _elaspedTimeInState = 0.0F;
                }
                break;
            case States.loopAction:
                Action();
                if (_elaspedTimeInState >= ActiveDuration)
                {
                    oldState = state;
                    state = States.endAction;
                    _elaspedTimeInState = 0.0F;
                }
                break;
            case States.endAction:
                if (_elaspedTimeInState >= EndActionDuration)
                {
                    this.EndAction();
                    oldState = state;
                    state = States.iddle;
                    _elaspedTimeInState = 0.0F;
                }
                break;
        }
    }

    public void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.gameObject != this && collider.gameObject != Parent)
        {
            if (!_inRangeObjects.Contains(collider.gameObject))
            {
                _inRangeObjects.Add(collider.gameObject);
            }
        }
    }

    public void OnTriggerExit2D(Collider2D collider)
    {

        if (_inRangeObjects.Contains(collider.gameObject))
        {
            _inRangeObjects.Remove(collider.gameObject);
        }
        //if (collider.gameObject != this && collider.gameObject != Parent)
        //{
        //    _inRangeObjects.Remove(collider.gameObject);
        //}
    }



    public abstract bool CanUse();


    public virtual Vector2 GetHitDirection()
    {
        Vector2 joystickVector = new Vector2(Player.xpad.LeftJoystickHorizontal.Value, -Player.xpad.LeftJoystickVertical.Value);
        if (!joystickVector.Equals(Vector2.zero) && Mathf.Abs(Vector2.Angle(joystickVector, HitDirectionVector * Mathf.Sign(Player.playerDirection.x))) <= angleOffset)
        {
            Debug.Log("HitDirection : " + joystickVector);
            return joystickVector;
        }
        else
        {
            Vector2 directionVector = new Vector2(HitDirectionVector.x * _lastDirectionVector.x, HitDirectionVector.y);
            Debug.Log("HitDirectionLast : " + directionVector);
            return directionVector;
        }
    }

    public virtual void Use()
    {
        if (Player.OwnedBall != null)
        {
            Debug.Log("Player.OwnedBall " + Player.OwnedBall);
            Player.OwnedBall.HitBy(Player, this.GetHitDirection(), Force*50);
            Player.OwnedBall = null;
        }
        if (CanUse() && state != States.startAction && state != States.loopAction && state != States.endAction)
        {
            Debug.Log("Start Action");
            state = States.startAction;
            Player.disableBallCatcher(0.05f);
        }
        //taper
    }

    public virtual void PrepareAction()
    {
        
    }

    public virtual void Action()
    {
        isAction = true;
        foreach (GameObject go in _inRangeObjects)
        {
            Debug.Log(go);
        }
        foreach (Ball b in BallsManager.Instance.Balls)
        {
            if (_inRangeObjects.Contains(b.gameObject))
            {
                _hitResolver = b.GetComponent<HitResolverBall>();
                if (_hitResolver != null && !BallTriggered.ContainsKey(b) && b.Owner != Player)
                {
                    _hitResolver.Resolve(GetHitDirection(), Force*50, this.Player);
                    BallTriggered.Add(b, true);
                    Player.OwnedBall = null;
                    _inRangeObjects.Remove(b.gameObject);
                }
            }
        }
        foreach (HumanPLayer hp in HumanPlayersManager.Instance.HumanPLayers)
        {
            if (hp.Player.GetComponent<Collider2D>().bounds.Intersects(this.GetComponent<Collider2D>().bounds))
            {
                Debug.Log("Tape putois");
                _hitResolver = hp.Player.GetComponent<HitResolverPlayer>();
                if(_hitResolver==null)
                    Debug.Log("Prob hit resolver");

                if (_hitResolver != null && !PlayerTriggered.ContainsKey(hp.Player) && this.Player.Team != hp.Player.Team)
                {
                    Debug.Log("hit resolver find putois");
                    _hitResolver.Resolve(GetHitDirection(), Force, this.Player);
                    PlayerTriggered.Add(hp.Player, true);
                }
            }
        }
    }

    public virtual void EndAction()
    {
        CoolDownBegin = Time.time;
        isAction = false;
    }

    public bool isInCoolDown()
    {
        if (Time.time - CoolDownBegin > CoolddownDuration)
        {
            return false;      
        }
        else
            return true;
    }
}
