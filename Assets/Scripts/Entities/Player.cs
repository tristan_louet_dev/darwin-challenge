﻿﻿using Assets.Scripts.Managers;
﻿using Assets.Scripts.Managers.Events;
﻿using Assets.Scripts.Utils;
﻿using UnityEngine;
using Utils;
﻿using Action = Utils.Action;

namespace Assets.Scripts.Entities
{
    [RequireComponent(typeof(Rigidbody2D), typeof(CaracteristicsPlayer), typeof(HitResolverPlayer))]
    public class Player : MonoBehaviour
    {
        public CaracteristicsPlayer CaracteristicsPlayer
        {
            get;
            private set;
        }

        public HitResolverPlayer HitResolverPlayer { get; private set; }

        // [SerializeField]
        //private Transform _groundCheck;
        public bool OnGround { get; private set; }

        [SerializeField]
        private float _jumpCooldown = 0.01F;

        public float JumpCooldown { get { return _jumpCooldown; } }

        public float _jumpCooldownCounter { get; private set; }

        private Rigidbody2D _rigidbody;

        private float _xpadJoyHorVal;

        public Team Team { get; set; }

        public bool Stunned { get; set; }

        public Ball OwnedBall { get; set; }

        private int _goals;
        public int GetGoals { get { return _goals; } }

        [SerializeField]
        private BodyPart _head;

        public BodyPart Head { get { return _head; } }

        [SerializeField]
        private GroundCheck_v2 _groundCheck = null;

        public GroundCheck_v2 GroundCheck { get { return _groundCheck; } }

        [SerializeField]
        private BodyPart _chest;

        public BodyPart Chest
        {
            get { return _chest; }
        }

        [SerializeField]
        private BodyPart _legs;

        public BodyPart Legs
        {
            get { return _legs; }
        }

        [SerializeField]
        [Range(1f, 10000f)]
        private float _ForceJump = 0.0f;

        [SerializeField]
        [Range(1f, 20f)]
        private float _maxSpeed = 0.0f;

        [SerializeField]
        [Range(1f, 100f)]
        private float _gripRange = 0.0f;

        [SerializeField]
        [Range(0, 1f)]
        private float _inertia = 0.0f;

        [SerializeField]
        [Range(0f, 10f)]
        private float _resistance = 0.0f;

        [SerializeField]
        [Range(0f, 1f)]
        private float _airControl = 0.0f;

        [SerializeField]
        [Range(0f, 1f)]
        private float _floorFriction = 0.0f;

        [SerializeField]
        [Range(0f, 20f)]
        private float _acceleration = 0.0f;

        [SerializeField]
        private ForceMode2D _jumpForceMode = ForceMode2D.Impulse;

        private bool _isColliding;

        private float TimerStun;

        public X360GamePad xpad;

        public float vitesse;

        public Vector2 playerDirection;

        public BallCatcher ballCatcher;

        private ContextualAction _jumpAction;
        private ContextualAction _legsAction;
        private ContextualAction _chestAction;
        private ContextualAction _headAction;

        void Start()
        {
            _goals = 0;
            _isColliding = false;
            playerDirection = Vector2.right;
            CaracteristicsPlayer = GetComponent<CaracteristicsPlayer>();
            HitResolverPlayer = GetComponent<HitResolverPlayer>();
            _rigidbody = GetComponent<Rigidbody2D>();
            Stunned = false;
            _jumpCooldownCounter = 0.0F;

            _legsAction = new ContextualAction(xpad.B, GameStateManager.GameState.game);
            _legsAction.Callback = () =>
            {
                if (!Stunned)
                    Legs.Use();
            };
            InputManager.Instance.Add(_legsAction);

            _headAction = new ContextualAction(xpad.Y, GameStateManager.GameState.game);
            _headAction.Callback = () =>
            {
                if (!Stunned)
                    Head.Use();
            };
            InputManager.Instance.Add(_headAction);

            _chestAction = new ContextualAction(xpad.X, GameStateManager.GameState.game);
            _chestAction.Callback = () =>
            {
                if (!Stunned)
                    Chest.Use();
            };
            InputManager.Instance.Add(_chestAction);

            _jumpAction = new ContextualAction(xpad.A, GameStateManager.GameState.game);
            _jumpAction.Callback = () =>
            {
                Debug.Log("Jump : _jumpCooldownCounter = " + _jumpCooldownCounter);
                if (OnGround && !Stunned && _jumpCooldownCounter <= 0)
                {
                    _rigidbody.AddForce(new Vector2(0, GetForceJump()), _jumpForceMode);

                    //ResetJumpCooldown();
                }
            };
            InputManager.Instance.Add(_jumpAction);

            _rigidbody.gravityScale = 7;
            _rigidbody.mass = 60;
            EventManager.OnGameStarting += () =>
            {
                SpawnerManager.Instance.Spawn(this);
            };
        }

        void Update()
        {
            //#region onwayplatforms

            //Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"),
            //                    LayerMask.NameToLayer("OneWayPlatform"),
            //                    _rigidbody.velocity.y > 0
            //                   );
            //#endregion


        }

        void FixedUpdate()
        {
            #region GoundCheck
            //Debug.Log("_jumpCooldownCounter = " + _jumpCooldownCounter + " ; OnGround = " + OnGround);
            if (_jumpCooldownCounter >= 0) _jumpCooldownCounter -= Time.deltaTime;
            OnGround = _groundCheck.GroundCheck;

            #endregion

            //OnGround = (Physics2D.Linecast(transform.position, _groundCheck.position, ~LayerMask.GetMask("Player", "Ball", "BodyPart", "Tongue","Fart")).collider != null);

            if (!Stunned)
            {
                #region Deplacement
                if (xpad != null && xpad.LeftJoystickHorizontal != null) _xpadJoyHorVal = xpad.LeftJoystickHorizontal.Value;
                playerDirection = new Vector2(_xpadJoyHorVal, 0);
                if (OnGround)
                {
                    this.vitesse = _xpadJoyHorVal * GetAcceleration() + this.vitesse * GetInertia() * (1.0F - _floorFriction);
                }
                else
                {
                    this.vitesse = _xpadJoyHorVal * GetAcceleration() * GetAirControl() + this.vitesse * GetInertia();
                }
                if (playerDirection.x < 0)
                {
                    this.transform.localScale = new Vector2(-Mathf.Abs(this.transform.localScale.x),
                        this.transform.localScale.y);
                }
                if (playerDirection.x > 0)
                {
                    this.transform.localScale = new Vector2(Mathf.Abs(this.transform.localScale.x),
                        this.Head.transform.localScale.y);
                }
                //Old
                //if (xpad.LeftJoystickHorizontal.Value > 0)
                //{
                //    if (OnGround)
                //    {
                //        this.vitesse = GetAcceleration() + this.vitesse * GetInertia()/2;
                //    }
                //    else
                //    {
                //        this.vitesse = GetAcceleration() * GetAirControl() + this.vitesse * GetInertia();
                //    }
                //    if (playerDirection.x < 0)
                //    {
                //        this.transform.localScale = new Vector2(Mathf.Abs(this.transform.localScale.x),
                //            this.transform.localScale.y);
                //    }
                //    playerDirection = new Vector2(xpad.LeftJoystickHorizontal.Value, 0);
                //}
                //else if (xpad.LeftJoystickHorizontal.Value < 0)
                //{
                //    if (OnGround)
                //        this.vitesse = -GetAcceleration() + this.vitesse * GetInertia();
                //    else
                //        this.vitesse = -GetAcceleration() * GetAirControl() + this.vitesse * GetInertia();

                //    if (playerDirection.x > 0)
                //    {
                //        this.transform.localScale = new Vector2(-Mathf.Abs(this.transform.localScale.x),
                //            this.Head.transform.localScale.y);
                //    }
                //    playerDirection = new Vector2(xpad.LeftJoystickHorizontal.Value, 0);
                //}
                else
                {
                    //this.vitesse = this.vitesse * GetInertia();
                }

                //TODO : Ne pas depasser vitesseMax
                if (!this.Stunned)
                {
                    if (this.vitesse > _maxSpeed)
                        this.vitesse = _maxSpeed;
                    else if (this.vitesse < -_maxSpeed)
                        this.vitesse = -_maxSpeed;
                } else
                {
                    if (this.vitesse > _maxSpeed*2)
                        this.vitesse = _maxSpeed*2;
                    else if (this.vitesse < -_maxSpeed*2)
                        this.vitesse = -_maxSpeed*2;
                }



            }
            else
            {
                if (Time.time - TimerStun > 1)
                {
                    this.Stunned = false;
                }
            }

            if (!this.Stunned)
            {

                if (_isColliding && !OnGround)
                {
                    this.vitesse = 0.0F;
                    //_rigidbody.velocity = new Vector2(_rigidbody.velocity.x, -20);
                    _rigidbody.velocity = new Vector2(this.vitesse, _rigidbody.gravityScale * Time.deltaTime + _rigidbody.velocity.y);
                }

                //Debug.Log("_isColliding " + _isColliding);

                _rigidbody.velocity = new Vector2(this.vitesse, _rigidbody.velocity.y);
            }
                #endregion

        }

        void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.tag == "Ball")
            {
                var b = other.gameObject.GetComponent<Ball>();
                if ((OwnedBall == null) && b.TryToOwn(this))
                {
                    OwnedBall = b;
                    b.OnOwnerChange += OnOwnedBallOwnerChange;
                }
            }
        }

        void OnCollisionStay2D(Collision2D other)
        {
            if (other.collider.gameObject.layer == LayerMask.NameToLayer("Floor") || other.collider.gameObject.layer == LayerMask.NameToLayer("OneWayPlatform"))
            {
                //TODO : find a better fix for wall sticking
                _isColliding = true;
            }
        }

        void OnCollisionExit2D(Collision2D other)
        {

            if (other.collider.gameObject.layer == LayerMask.NameToLayer("Floor") || other.collider.gameObject.layer == LayerMask.NameToLayer("OneWayPlatform"))
            {
                _isColliding = false;
            }
        }

        private void OnOwnedBallOwnerChange(Player oldOwner, Player newOwner)
        {
            OwnedBall.OnOwnerChange -= OnOwnedBallOwnerChange;
            OwnedBall = null;
        }

        public void SetStunned()
        {
            Stunned = true;
            if (OwnedBall != null)
            {
                OwnedBall.HitBy(this, new Vector2(0, 1), 0.1f);
            }
            TimerStun = Time.time;
        }

        public void SetBodies(GameObject head, GameObject chest, GameObject legs)
        {
            GameObject headInst = Instantiate(head) as GameObject;
            headInst.transform.parent = gameObject.transform;
            headInst.transform.localPosition = new Vector2(-0.12F, 0.45F);
            _head = headInst.GetComponent<BodyPart>();
            _head.Parent = this.gameObject;
            _head.Player = gameObject.GetComponent<Player>();

            GameObject chestInst = Instantiate(chest) as GameObject;
            chestInst.transform.parent = gameObject.transform;
            chestInst.transform.localPosition = new Vector2(0.0F, 0.0F);
            _chest = chestInst.GetComponent<BodyPart>();
            _chest.Parent = this.gameObject;
            _chest.Player = gameObject.GetComponent<Player>();

            GameObject legsInst = Instantiate(legs) as GameObject;
            legsInst.transform.parent = gameObject.transform;
            legsInst.transform.localPosition = new Vector2(-0.16F, -0.35F);
            _legs = legsInst.GetComponent<BodyPart>();
            _chest.Parent = this.gameObject;
            _legs.Player = gameObject.GetComponent<Player>();
        }

        public void ApplyDamage()
        {

        }

        public void SetBall(Ball b)
        {
            OwnedBall = b;

            b.OnOwnerChange += OnOwnedBallOwnerChange;
        }

        private void ResetJumpCooldown()
        {
            _jumpCooldownCounter = _jumpCooldown;
        }

        public float GetMaxSpeed()
        {
            return _maxSpeed * CaracteristicsPlayer.MaxSpeedCoef;
        }

        public float GetForceJump()
        {
            return _ForceJump * CaracteristicsPlayer.ForceJumpCoef;
        }

        public float GetAcceleration()
        {
            return _acceleration * CaracteristicsPlayer.AccelerationCoef;
        }

        public float GetAirControl()
        {
            return _airControl * CaracteristicsPlayer.AirControlCoef;
        }

        public float GetGripRange()
        {
            return _gripRange * CaracteristicsPlayer.GripRangeCoef;
        }

        public float GetInertia()
        {
            return 0.90f + (_inertia * CaracteristicsPlayer.InertiaCoef) / 10;
        }

        public float GetResistance()
        {
            return _resistance * CaracteristicsPlayer.ResistanceCoef;
        }

        //goals
        public void Goaled()
        {
            _goals++;
        }

        public void ResetGoals()
        {
            _goals = 0;
        }

        public void removeActions()
        {
            InputManager.Instance.Remove(_headAction);
            InputManager.Instance.Remove(_chestAction);
            InputManager.Instance.Remove(_legsAction);
            InputManager.Instance.Remove(_jumpAction);
        }

        void OnDestroy()
        {
            EventManager.OnGameStarting -= () =>
            {
                SpawnerManager.Instance.Spawn(this);
            };
            print("prout");
        }

        public void disableBallCatcher(float seconds)
        {
            ballCatcher.canCatch = false;
            Invoke("enableBallCatcher", 0.1f); // ugly as fuck
        }

        public void enableBallCatcher()
        {
            ballCatcher.canCatch = true;
        }
    }
}
