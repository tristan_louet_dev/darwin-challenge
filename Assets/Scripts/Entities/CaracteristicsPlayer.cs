﻿using UnityEngine;
using Assets.Scripts.Entities;

[RequireComponent(typeof(Player))]
public class CaracteristicsPlayer : MonoBehaviour, ICaracteristics
{

    private Player _player;

    void Start()
    {
       _player = GetComponent<Player>();
    }

    public float ForceJumpCoef
    {
        get
        {
            return _player.Head.CaracteristicsBodyPart.ForceJumpCoef +
                  _player.Chest.CaracteristicsBodyPart.ForceJumpCoef +
                  _player.Legs.CaracteristicsBodyPart.ForceJumpCoef+1;
        }
    }

    public float CoefSpeed = 1;
    public float MaxSpeedCoef
    {
        get
        {
            return _player.Head.CaracteristicsBodyPart.MaxSpeedCoef +
                    _player.Chest.CaracteristicsBodyPart.MaxSpeedCoef +
                    _player.Legs.CaracteristicsBodyPart.MaxSpeedCoef+CoefSpeed;
         
        }
    }

    public float GripRangeCoef
    {
        get
        {
            return _player.Head.CaracteristicsBodyPart.GripRangeCoef +
                 _player.Chest.CaracteristicsBodyPart.GripRangeCoef +
                 _player.Legs.CaracteristicsBodyPart.GripRangeCoef+1;
        }
    }

    public float InertiaCoef
    {
        get
        {
            return _player.Head.CaracteristicsBodyPart.InertiaCoef +
                   _player.Chest.CaracteristicsBodyPart.InertiaCoef +
                   _player.Legs.CaracteristicsBodyPart.InertiaCoef+1;
        }
    }

    public float ResistanceCoef
    {
        get
        {
            return _player.Chest.CaracteristicsBodyPart.ResistanceCoef +
                   _player.Head.CaracteristicsBodyPart.ResistanceCoef +
                   _player.Legs.CaracteristicsBodyPart.ResistanceCoef+1;
        }
    }

    public float AirControlCoef
    {
        get
        {
            return _player.Head.CaracteristicsBodyPart.AirControlCoef +
                   _player.Chest.CaracteristicsBodyPart.AirControlCoef +
                   _player.Legs.CaracteristicsBodyPart.AirControlCoef+1;
        }
    }

    public float AccelerationCoef {
        get
        {
            return _player.Head.CaracteristicsBodyPart.AccelerationCoef +
                   _player.Chest.CaracteristicsBodyPart.AccelerationCoef +
                   _player.Legs.CaracteristicsBodyPart.AccelerationCoef+1;
        }
    }
}
