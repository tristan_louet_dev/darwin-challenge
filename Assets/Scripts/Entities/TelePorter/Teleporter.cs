﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Entities;

public class Teleporter : MonoBehaviour {

    //public Vector2 normal;

    public Teleporter End;

    private List<GameObject> _teleportedObject;

	// Use this for initialization
	void Start () {
	    _teleportedObject = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (!_teleportedObject.Contains(other.gameObject))
            {
                End.Teleport(other.gameObject);
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (_teleportedObject.Contains(other.gameObject))
            {
                _teleportedObject.Remove(other.gameObject);
                try
                {
                    other.gameObject.GetComponent<Player>().OwnedBall.gameObject.GetComponent<TrailRenderer>().enabled = true;
                }
                catch
                {
                    Debug.Log("NOOOOOON");
                }
            }
        }
    }

    public void Teleport(GameObject go)
    {
        _teleportedObject.Add(go);
        try
        {
            go.GetComponent<Player>().OwnedBall.gameObject.GetComponent<TrailRenderer>().enabled = false;
            go.GetComponent<Player>().OwnedBall.gameObject.transform.position = transform.position;
        }
        catch
        {
            Debug.Log("NOOOOOON");
        }
        go.transform.position = transform.position;
    }
}
