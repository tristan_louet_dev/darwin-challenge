﻿using System.Collections.Generic;
using Assets.Scripts.Managers;
using Assets.Scripts.Managers.Events;
using Audio;
using UnityEngine;

namespace Assets.Scripts.Entities
{
    [RequireComponent(typeof (Collider2D), typeof (AudioData))]
    public class Goal : MonoBehaviour
    {
        private struct CapturedObject
        {
            public GameObject obj;
            public Vector2 velocity;
        }
        [SerializeField] private Team _owningTeam = Team.None;
        private List<CapturedObject> _capturedObjects;

        void Awake()
        {
            _capturedObjects = new List<CapturedObject>();
        }
        
        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.tag == "Ball")
            {
                var ball = other.GetComponent<Ball>();
                Debug.Log("Last team to hit the ball was " + ball.LastTeamToHit);
                if (_owningTeam == ball.LastTeamToHit)
                {
                    Debug.Log("Some dumbass goal against its own team (" + _owningTeam + ")");
                    // But contre son camp ==> point pour toutes les autres équipes
                    foreach (var team in _owningTeam.OppositeTeams())
                    {
                        ScoreManager.Instance[team] += 1;
                        EventManager.OnGoal.Fire(ball.LastOwner, ball, this, team);
                    }
                }
                else // but par une autre équipe
                {
                    ScoreManager.Instance[ball.LastTeamToHit] += 1;
                    ball.LastOwner.Goaled();
                    EventManager.OnGoal.Fire(ball.LastOwner, ball, this, ball.LastTeamToHit);
                }
                _capturedObjects.Add(new CapturedObject {obj = ball.gameObject, velocity = Vector2.zero});
                ball.GetComponent<Collider2D>().enabled = false;
                ball.GetComponent<Rigidbody2D>().isKinematic = true;
                var ad = GetComponent<AudioData>();
                if (ad != null)
                {
                    AudioManager.Instance.Play(ad);
                }
            }
        }

        void Update()
        {
            var toDeleteIndices = new Queue<CapturedObject>();
            for (int i = 0; i < _capturedObjects.Count; ++i)
            {
                var co = _capturedObjects[i];
                if (co.obj == null)
                {
                    toDeleteIndices.Enqueue(co);
                    continue;
                }
                var v = co.velocity;
                co.obj.transform.position = Vector2.SmoothDamp(co.obj.transform.position,
                    transform.position, ref v, 0.1f);
                co.velocity = v;
            }
            while (toDeleteIndices.Count > 0)
            {
                _capturedObjects.Remove(toDeleteIndices.Dequeue());
            }
        }
    }
}
