﻿using Assets.Utils;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    class GameInitializer : MonoBehaviour
    {
        public string beginLevel = "main_menu";

        void Start()
        {
            AutoFade.LoadLevel(beginLevel, 0.5f, 0.5f, Color.black); // launch level "level"
        }
    }
}
