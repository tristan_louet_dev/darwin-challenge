﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Entities;

public abstract class HitResolver : MonoBehaviour
{
	protected Rigidbody2D _rigidbody;

	void Start()
	{
		_rigidbody = GetComponent<Rigidbody2D> ();
	}

    public abstract void Resolve(Vector2 direction, float force, Player p);
}
