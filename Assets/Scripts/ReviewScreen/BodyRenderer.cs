﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.ReviewScreen
{
    class BodyRenderer : MonoBehaviour
    {
        public GameObject headRenderer = null;
        public GameObject chestRenderer = null;
        public GameObject legsRenderer = null;

        public void setSprites(Sprite headSprite, Sprite chestSprite, Sprite legsSprite)
        {
            headRenderer.GetComponent<SpriteRenderer>().sprite = headSprite;
            chestRenderer.GetComponent<SpriteRenderer>().sprite = chestSprite;
            legsRenderer.GetComponent<SpriteRenderer>().sprite = legsSprite;
        }
    }
}
