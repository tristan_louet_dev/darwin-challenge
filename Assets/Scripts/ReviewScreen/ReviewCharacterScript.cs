﻿using Assets.Scripts.Utils;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.ReviewScreen
{
    class ReviewCharacterScript : MonoBehaviour
    {
        public GameObject bodyDisplayer = null;
        public GameObject pictureBackground = null;
        public GameObject pictureTop = null;
        public GameObject bestPlayerPostIt = null;

        public GameObject goalText = null;
        public GameObject deathText = null;

        public int _goals = 0;
        public int _deaths = 0;

        public bool _isInBlueTeam = true;
        public bool _isBestPlayer = false;

        public void SetReviewCharacterInformations(int goals, int deaths, bool isInBlueTeam)
        {
            _goals = goals;
            _deaths = deaths;
            _isInBlueTeam = isInBlueTeam;
            setHandWrittingText(goalText, _goals.ToString(), _goals);
            setHandWrittingText(deathText, _deaths.ToString(), _deaths);
            bestPlayerPostIt.SetActive(false);
            setPictureSprites();
        }

        public void SetReviewCharacterSprites(Sprite headSprite, Sprite chestSprite, Sprite legsSprite)
        {
            bodyDisplayer.GetComponent<BodyRenderer>().setSprites(headSprite, chestSprite, legsSprite);
        }

        public void SetReviewCharacterBestPlayer()
        {
            _isBestPlayer = true;
            bestPlayerPostIt.SetActive(true);
        }

        public void setHandWrittingText(GameObject textRenderer, string text, int fontSizeModification = 0)
        {
            textRenderer.GetComponent<Text>().fontSize += fontSizeModification;
            textRenderer.GetComponent<Text>().text = text;
        }

        public void setPictureSprites()
        {
            int spriteIndexMax = Mathf.Min(pictureBackground.GetComponent<SpriteCollectionManager>().getSpritesNumber(),
                                        pictureTop.GetComponent<TwoTeamSpriteCollectionManager>().getTeamSpritesNumber(_isInBlueTeam));

            print(spriteIndexMax);

            int spriteIndex = Random.Range(0, spriteIndexMax);

            print(spriteIndex);

            pictureBackground.GetComponent<SpriteCollectionManager>().SetSpriteInCollection(spriteIndex);
            pictureTop.GetComponent<TwoTeamSpriteCollectionManager>().SetTeamSpriteInCollection(_isInBlueTeam, spriteIndex);
        }
    }
}
