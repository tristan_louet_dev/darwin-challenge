﻿using Assets.Scripts.Entities;
using Assets.Scripts.Managers;
using Assets.Scripts.Managers.Events;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    class ScoreDisplayerScript : MonoBehaviour
    {
        public GameObject timeValueText = null;
        public GameObject blueTeamScoreText = null;
        public GameObject yellowTeamScoreText = null;

        public int baseScoreFontSize = 1;

        private int _secondsPassed = 0;

        void Start()
        {
            EventManager.OnGoal += updateScore;
            _secondsPassed = 0;
            InvokeRepeating("UpdateTime", 0.0f, 1.0f);
        }

        void updateScore(Player player, Ball ball, Goal goal, Team team)
        {
            setScore(ScoreManager.Instance[team].Value, team == Team.Blue);
        }

        public void setScore(uint score, bool isBlueTeam)
        {
            if(isBlueTeam)
            {
                blueTeamScoreText.GetComponent<Text>().fontSize = baseScoreFontSize + (int)score;
                blueTeamScoreText.GetComponent<Text>().text = score.ToString();
            }
            else
            {
                yellowTeamScoreText.GetComponent<Text>().fontSize = baseScoreFontSize + (int)score;
                yellowTeamScoreText.GetComponent<Text>().text = score.ToString();
            }
        }

        void UpdateTime ()
        {
            _secondsPassed ++;
            int minutes = ((int)_secondsPassed) / 60;
            int seconds = ((int)_secondsPassed) % 60;

            timeValueText.GetComponent<Text>().text = (minutes < 10 ? "0" : "") + minutes.ToString() + ":" + (seconds < 10 ? "0" : "") + seconds.ToString();
        }
    }
}
