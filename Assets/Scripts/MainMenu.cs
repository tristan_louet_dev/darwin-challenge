﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Utils;
using Audio;

public class MainMenu : MonoBehaviour
{
    public enum Action { newgame, options, credits, exit };

    [System.Serializable]
    public struct DiageticButton
    {
        public string Name;
        public Action Action;
        public List<GameObject> FeedBacksHover;
        public List<GameObject> FeedBacksClicked;
        public GameObject clickedSound;
        public string PreviousDiageticButtonName;
        public string NextDiageticButtonName;
    }

    public DiageticButton[] diageticButtons;

    private Dictionary<string, DiageticButton> diageticButtonsDic;

    public string selectedButtonName;

    // Use this for initialization
    void Start()
    {
        diageticButtonsDic = new Dictionary<string, DiageticButton>();

        
        foreach (DiageticButton db in diageticButtons)
        {
            diageticButtonsDic.Add(db.Name, db);
        }

        UnselectAll();

        Select(selectedButtonName);
    }

    public void UnselectAll()
    {
        foreach (DiageticButton db in diageticButtonsDic.Values)
        {
            foreach (GameObject fbh in db.FeedBacksHover)
            {
                fbh.SetActive(false);
            }
        }
    }

    public void Select(string newSelected) //highlight le bouton
    {
        selectedButtonName = newSelected;
        foreach (GameObject fbh in diageticButtonsDic[selectedButtonName].FeedBacksHover)
        {
            fbh.SetActive(true);
        }
    }

    public void Next() //highlight le bouton suivant
    {
        UnselectAll();
        Select(diageticButtonsDic[selectedButtonName].NextDiageticButtonName);
    }

    public void Previous() //highlight le bouton précédant
    {
        UnselectAll();
        Select(diageticButtonsDic[selectedButtonName].PreviousDiageticButtonName);
    }

    public void Click() // active le bouton, et fait l'action
    {
        DoAction(diageticButtonsDic[selectedButtonName].Action);
        if (diageticButtonsDic[selectedButtonName].clickedSound.GetComponent<AudioData>() != null)
        {
            AudioManager.Instance.Play(diageticButtonsDic[selectedButtonName].clickedSound.GetComponent<AudioData>());
        }
    }

    public void DoAction(Action action) // fait l'action
    {
        switch (action)
        {
            case Action.newgame:
                Debug.Log("action : newgame");
                AutoFade.LoadLevel("characterSelection", 0.5f, 0.5f, Color.black);
                break;
            case Action.options:
                Debug.Log("action : options");
                //AutoFade.LoadLevel("options", 0.5f, 0.5f, Color.black);
                break;
            case Action.credits:
                Debug.Log("action : credits");
                AutoFade.LoadLevel("credits", 0.5f, 0.5f, Color.black);
                break;
            case Action.exit:
                Debug.Log("action : exit");
                #if UNITY_EDITOR
                                UnityEditor.EditorApplication.isPlaying = false;
                #elif UNITY_WEBPLAYER
                         Application.OpenURL(webplayerQuitURL);
                #else
                         Application.Quit();
                #endif
                // old //Application.Quit(); // /!\ ne fonctionne pas sur l'éditeur, faut faire un build
                break;
            default:
                Debug.Log("action doesn't exist");
                break;
        }
    }
}
