﻿using Assets.Scripts.Entities;
using Assets.Scripts.Managers;
using UnityEngine;
using Audio;

public class Eagle : BodyPart
{
    public GameObject dashSound;

    private AudioData _dashSoundData;

    private bool _dashed;
    private float _dashDuration;
    private bool _wasInCoolDown;

	// Use this for initialization
	protected override void Start () {
	    base.Start();
	    _dashed = false;
	    _wasInCoolDown = false;

        _dashSoundData = dashSound.GetComponent<AudioData>();
	}
	
	// Update is called once per frame
    protected override void Update () {
        base.Update();
        if (_wasInCoolDown && !isInCoolDown())
        {
            _wasInCoolDown = false;
            SwitchToAnimationState("openWings", false);
        }
    }

    public override bool CanUse()
    {
        return (!this.isInCoolDown());
    }

    public override void Use()
    {
        if (CanUse() && state != States.startAction && state != States.loopAction && state != States.endAction)
        {
            base.Use();
        }
    }

    public override void EndAction()
    {
        base.EndAction();
        this._dashed = false;
        this.Player.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
        this.Player.Stunned = false;
        SwitchToAnimationState("idle-cooldown", false);
        _wasInCoolDown = true;
    }

    public override void Action()
    {
        if (!_dashed)
        {
            SwitchToAnimationState("dash", false);
            AudioManager.Instance.Play(_dashSoundData);
            this._dashed = true;
            this.Player.SetStunned();
            Debug.Log("Dash : " + GetHitDirection());
            this.Player.GetComponent<Rigidbody2D>().velocity = new Vector2(GetHitDirection().x * 30, GetHitDirection().y * 30);
        }
        foreach (Ball b in BallsManager.Instance.Balls)
        {
            if (b.GetComponent<Collider2D>().bounds.Intersects(this.GetComponent<Collider2D>().bounds))
            {
                _hitResolver = b.GetComponent<HitResolverBall>();
                if (_hitResolver != null && !BallTriggered.ContainsKey(b))
                {
                    _hitResolver.Resolve(new Vector2(GetHitDirection().x, GetHitDirection().y), Force*1.2F, this.Player);
                    BallTriggered.Add(b, true);
                    AudioManager.Instance.Play(_hitBallSoundData);
                }
            }
        }
        foreach (HumanPLayer hp in HumanPlayersManager.Instance.HumanPLayers)
        {
            if (hp.Player.GetComponent<Collider2D>().bounds.Intersects(this.GetComponent<Collider2D>().bounds))
            {
                _hitResolver = hp.Player.GetComponent<HitResolverPlayer>();
                if (_hitResolver != null && !PlayerTriggered.ContainsKey(hp.Player) && this.Player.Team != hp.Player.Team)
                {
                    _hitResolver.Resolve(new Vector2(GetHitDirection().x, -GetHitDirection().y), Force * 2, this.Player);
                    PlayerTriggered.Add(hp.Player, true);
                    AudioManager.Instance.Play(_hitBallSoundData);
                }
            }
        }
    }

    public override void PrepareAction()
    {
        this.Player.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
        SwitchToAnimationState("prepareDash", false);
    }

    public override Vector2 GetHitDirection()
    {
        return base.GetHitDirection();
    }
}
