﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Assets.Scripts.Entities;
using Assets.Scripts.Managers;
using Utils;
using Action = Utils.Action;
using Audio;

public class Spider : BodyPart
{

    public GameObject Projectile;

	// Use this for initialization
	protected override void Start () {
	    base.Start();
	}

    protected override void Update()
    {
        base.Update();

    }

    public override bool CanUse()
    {
        return (!this.isInCoolDown());
    }

    public override void Use()
    {
    }

    public void StopUse()
    {
    }

    public override void Action()
    {
        Debug.Log("JellyFish ACTION");
        foreach (Ball b in BallsManager.Instance.Balls)
        {
            if (b.GetComponent<Collider2D>().bounds.Intersects(this.GetComponent<Collider2D>().bounds))
            {
                _hitResolver = b.GetComponent<HitResolverBall>();
                if (_hitResolver != null)
                {
                    _hitResolver.Resolve((b.transform.position - transform.position).normalized, Force, Player);
                    //b.GetComponent<Rigidbody2D>().AddForce(new Vector2(10000,0), ForceMode2D.Impulse);
                    AudioManager.Instance.Play(_hitBallSoundData);
                }
            }
        }
        foreach (HumanPLayer hp in HumanPlayersManager.Instance.HumanPLayers)
        {
            if (hp.Player.GetComponent<Collider2D>().bounds.Intersects(this.GetComponent<Collider2D>().bounds))
            {
                _hitResolver = hp.Player.GetComponent<HitResolverPlayer>();
                if (_hitResolver != null && !PlayerTriggered.ContainsKey(hp.Player))
                {
                    Debug.Log("Trouver quelqu'un a taper");
                    _hitResolver.Resolve((hp.Player.transform.position - transform.position).normalized, Force, Player);
                    // hp.Player.GetComponent<Rigidbody2D>().AddForce(new Vector2(10000,0), ForceMode2D.Force);
                    PlayerTriggered.Add(hp.Player, true);
                    AudioManager.Instance.Play(_hitBallSoundData);
                }
            }
        }
    }

    public override Vector2 GetHitDirection()
    {
        return base.GetHitDirection();
    }

    void OnDestroy ()
    {
    }
}
