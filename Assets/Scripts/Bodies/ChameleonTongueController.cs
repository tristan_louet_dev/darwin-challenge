﻿using System.Security;
using Assets.Scripts.Managers.Events;
using UnityEngine;
using System.Collections;
using Assets.Scripts.Entities;
using Assets.Scripts.Managers;

public class ChameleonTongueController : MonoBehaviour
{

    public enum TongueState {usable = 1, reload, launched, gripped, cooldown}; // the different states of the Tongue

    public Chameleon chameleon;                     // the chameleon
    public ChameleonHeadController ChameleonHeadController;  // the  head controller
    public LineRenderer rope;                       // the rope
    public TongueState _state;
    public TongueState state {
        get { return _state; }
        private set
        {
            _state = value;
            OnStateChange.Fire(_state);
        }
    }     // Tongue is loaded by default
    public Event<TongueState> OnStateChange;
    private float _range;                     // Tongue distance before coming back (I know I should use a distance from the gun, but who cares ?)
    private float _tongueSpeed;                        // the speed of the bird named "Tongue"
    public float TongueSpeedReturn = 20.0F; // the speed of the bird named "Tongue"

    private Ball grippedBall = null;
    private Ball ballNear = null;
    private float distmin = 0.0F;

	// Use this for initialization
	void Awake ()
	{
        OnStateChange = new Event<TongueState>();
	    state = TongueState.usable;
	}
	
	// Update is called once per frame
	void Update () {
	    if (state == TongueState.usable || state == TongueState.cooldown)  //USABLE
        {
            transform.position = ChameleonHeadController.transform.position;
            transform.rotation = ChameleonHeadController.transform.rotation;
        }
        else if (state == TongueState.launched)   //LAUNCHED
        {
            if (BallsManager.Instance.Balls.Count > 0)
            {
                grippedBall = null;
                ballNear = null;
                distmin = 0.0F;

                distmin = Vector2.Distance(BallsManager.Instance.Balls[0].transform.position, transform.position);
                foreach (Ball ball in BallsManager.Instance.Balls)
                {
                    if (Vector2.Distance(ball.transform.position, transform.position) <= distmin)
                    {
                        distmin = Vector2.Distance(ball.transform.position, transform.position);
                        ballNear = ball;
                    }
                }
                if (ballNear != null && distmin <= 1)
                {
                    ballNear.RemoveOwner();
                    grippedBall = ballNear; 
                    state = TongueState.gripped;
                }
            }

            if (_range <= Mathf.Abs((transform.position - ChameleonHeadController.transform.position).magnitude))
            {
                state = TongueState.reload;
            }
        }
        else if (state == TongueState.reload)    //RELOAD
        {
            Reload((ChameleonHeadController.transform.position - transform.position).normalized, TongueSpeedReturn);
        }
        else if (state == TongueState.gripped)    //GRIPPED
        {
            if (grippedBall != null)
            {
                //Debug.Log("come back ball !");
                grippedBall.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                Reload((ChameleonHeadController.transform.position - transform.position).normalized, TongueSpeedReturn);
                grippedBall.transform.position = transform.position;
            }
        }
	}

    public void Launch(Vector2 direction, float speed, float range)
    {
        if (chameleon.Player.OwnedBall == null) //CanShoot(direction))
        {
            //Debug.Log("chameleon.Player.OwnedBall = null");
            rope.enabled = true;
            transform.parent = null;
            GetComponent<Rigidbody2D>().velocity = direction*speed;
            state = TongueState.launched;
            _range = range;
            _tongueSpeed = speed;
        }
        else
        {
            if (chameleon.Player.OwnedBall != null)
                chameleon.Player.OwnedBall.RemoveOwner();
            grippedBall = null;
        }
    }

    public void Reload(Vector2 direction, float speed)
    {
        //Debug.Log("Tongue come back dir " + direction * speed);
        GetComponent<Rigidbody2D>().velocity = direction * (speed + chameleon.Player.GetComponent<Rigidbody2D>().velocity.magnitude);
    }

    public void Arrived()
    {
        rope.enabled = false;
        state = TongueState.cooldown;
        //transform.parent = ChameleonHeadController.transform;
        grippedBall = null;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //if (other.CompareTag("Ball"))
        //{
        //    grippedBall = other.GetComponent<Ball>();
        //    state = TongueState.gripped;
        //    //other.GetComponent<HitResolverBall>().Resolve((PlayerRigidbody2D.transform.position - other.transform.position).normalized, chameleon.Force);
        //}
        //else if (state == TongueState.launched)
        //{
        //    Debug.Log(chameleon.transform.parent.gameObject);
        //    Debug.Log(other.gameObject);
        //    state = TongueState.loading;
        //}
    }

    public bool CanShoot(Vector2 direction)
    {
        return true;
    }

    public void setTongueState(TongueState newTongueState)
    {
        state = newTongueState;
    }

}
