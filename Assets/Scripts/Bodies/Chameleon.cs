﻿using UnityEngine;
using Assets.Scripts.Entities;
using Assets.Scripts.Managers;
using Audio;

public class Chameleon : BodyPart
{
    public GameObject launchTongueSound;
    public GameObject endCooldownSound;

    public GameObject Tongue;
    public GameObject head;
    public GameObject rope;
    public GameObject ring;
    public float range = 5.0F;
    public float speed = 10.0F;
    public float cooldown = 3.0F; //time between each attack

    private float cooldownLeft = 0.0F;

    private Vector3 ropePos1;
    private Vector3 ropePos2;

    private AudioData _launchTongueSoundData;
    private AudioData _endCooldownSoundData;

	// Use this for initialization
	protected override void Start () {
	    base.Start();
	    Tongue.GetComponent<ChameleonTongueController>().OnStateChange += (s) =>
	    {
	        switch (s)
	        {
	            case ChameleonTongueController.TongueState.gripped:
                    SwitchToAnimationState("returnTongue", true);
	                break;
	            case ChameleonTongueController.TongueState.reload:
	                SwitchToAnimationState("returnTongue", true);
	                break;
	            case ChameleonTongueController.TongueState.launched:
	                SwitchToAnimationState("fire", true);
	                break;
	            case ChameleonTongueController.TongueState.usable:
	                SwitchToAnimationState("idle", true);
	                break;
                case ChameleonTongueController.TongueState.cooldown:
                    SwitchToAnimationState("idle-cooldown", true);
                    break;

                default :
                    break;
	        }
	    };

        _launchTongueSoundData = launchTongueSound.GetComponent<AudioData>();
        _endCooldownSoundData = endCooldownSound.GetComponent<AudioData>();
	}
	
	// Update is called once per frame
    protected override void Update()
    {

        if (cooldownLeft > 0 && Tongue.GetComponent<ChameleonTongueController>().state == ChameleonTongueController.TongueState.cooldown)
	    {
	        cooldownLeft -= Time.deltaTime;
	        if (cooldownLeft <= 0)
	        {
                Tongue.GetComponent<ChameleonTongueController>().setTongueState(ChameleonTongueController.TongueState.usable);
	            SwitchToAnimationState("end-cooldown", true);
                AudioManager.Instance.Play(_endCooldownSoundData);
	        }
	    }

        ropePos1 = new Vector3(head.transform.position.x, head.transform.position.y, -0.1F);
        ropePos2 = new Vector3(ring.transform.position.x, ring.transform.position.y, -0.1F);

        //rope.GetComponent<LineRenderer>().enabled = true;

        rope.GetComponent<LineRenderer>().SetPosition(0, ropePos1);
        rope.GetComponent<LineRenderer>().SetPosition(1, ropePos2);

	}

    public override bool CanUse()
    {
        return (Tongue.GetComponent<ChameleonTongueController>().state == ChameleonTongueController.TongueState.usable) && BallsManager.Instance != null && BallsManager.Instance.Balls.Count > 0;
    }

    public override void Use()
    {
        print("USE CHAMELEON");
        float distmin = 0.0F;
        Ball ballNear = null;
        if (CanUse())
        {
            distmin = float.MaxValue;
            foreach (Ball ball in BallsManager.Instance.Balls)
            {
                if (Vector2.Distance(ball.transform.position, transform.position) <= distmin)
                {
                    distmin = Vector2.Distance(ball.transform.position, transform.position);
                    ballNear = ball;
                }
            }
            if (ballNear != null && distmin <= range)
            {
                (Tongue.GetComponent<ChameleonTongueController>()).Launch(
                    (ballNear.transform.position - head.transform.position).normalized,
                    speed, range);
                cooldownLeft = cooldown;
            }
            //else
            //{
            //    (Tongue.GetComponent<ChameleonTongueController>()).Launch(
            //        new Vector2(Player.xpad.LeftJoystickHorizontal.Value, -Player.xpad.LeftJoystickVertical.Value),
            //        speed, range);
            //    cooldownLeft = cooldown;
            //}

            Player.disableBallCatcher(0.05f);

            AudioManager.Instance.Play(_launchTongueSoundData);
        }
        base.Use();
    }

    public override void Action()
    {
        throw new System.NotImplementedException();
    }

    public override Vector2 GetHitDirection()
    {
        return base.GetHitDirection();
        //throw new System.NotImplementedException();
    }
}
