﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Audio;

public class Polecat : BodyPart
{
    public float cooldownFart = 2.0f;
    public GameObject tailSound;
    public GameObject fartSound;

    private float beginCoolDownFart;
    [SerializeField]
    private GameObject prefabFart = null;
    private bool wasJumpingUp = false;
    private bool wasFalling = false;

    private bool canFart = true;

    private AudioData _tailSoundData;
    private AudioData _fartSoundData;

	// Use this for initialization
    protected override void Start()
    {
	    base.Start();
	    beginCoolDownFart = -cooldownFart;
        _tailSoundData = tailSound.GetComponent<AudioData>();
        _fartSoundData = fartSound.GetComponent<AudioData>();
	}
	
	// Update is called once per frame
    protected override void Update()
    {
        if (!canFart && Time.time - beginCoolDownFart > cooldownFart)
        {
            canFart = true;
            print("CAN FART NOW");
        }

        base.Update();
        
        if (state == States.iddle)
        {
            var rigidbody = Player.GetComponent<Rigidbody2D>();
            if (!wasJumpingUp && rigidbody.velocity.y > 0 && !Player.OnGround)
            {
                Debug.Log("jump go ");
                wasJumpingUp = true;
                SwitchToAnimationState("jumpAscending" + (!canFart ? "-cooldown" : ""), true);
            }
            else if (!wasFalling && rigidbody.velocity.y < 0 && !Player.OnGround)
            {
                wasFalling = true;
                SwitchToAnimationState("jumpDescending" + (!canFart ? "-cooldown" : ""), true);
            }

            if (Player.OnGround)
            {
                wasFalling = false;
                wasJumpingUp = false;
                if (Mathf.Abs(rigidbody.velocity.x) > 0.25f)
                {
                    if (!isAction)
                        SwitchToAnimationState("run" + (!canFart ? "-cooldown" : ""), true);
                }
                else
                {
                    if (!isAction)
                        SwitchToAnimationState("idle" + (!canFart ? "-cooldown" : ""), true);
                }
            }
        }
    }

    public override bool CanUse()
    {
        return !isInCoolDown();
    }

    public override void Use()
    {
        if (CanUse() && state != States.startAction && state != States.loopAction && state != States.endAction)
        {
            base.Use();
            SwitchToAnimationState("tailAttack", true);
            AudioManager.Instance.Play(_tailSoundData);
        }
    }

    public override void Action()
    {
        base.Action();
        if (Time.time - beginCoolDownFart > cooldownFart)
        {
            beginCoolDownFart = Time.time;
            var fart = Instantiate(prefabFart).GetComponent<Fart>();
            fart.transform.position = transform.position;
            fart.Parent = this.Player;
            canFart = false;
            AudioManager.Instance.Play(_fartSoundData);
        }
    }

    public override Vector2 GetHitDirection()
    {
        return base.GetHitDirection();
    }
}
