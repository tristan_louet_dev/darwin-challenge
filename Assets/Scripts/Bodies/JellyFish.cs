﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Assets.Scripts.Entities;
using Assets.Scripts.Managers;
using Utils;
using Action = Utils.Action;
using Audio;

public class JellyFish : BodyPart
{
    public GameObject inflateSound;
    public GameObject deflateSound;

    private AudioData _inflateSoundData;
    private AudioData _deflateSoundData;

    private bool _fly = false;

    private ContextualAction _deflateAction;

	// Use this for initialization
    protected override void Start()
    {
	    base.Start();

        _deflateAction = new ContextualAction(Player.xpad.Y, ActionType.Released, 0.0f, GameStateManager.GameState.game);
        _deflateAction.Callback  = () =>
        {
            this.StopUse();
        };
        InputManager.Instance.Add(_deflateAction);

        _inflateSoundData = inflateSound.GetComponent<AudioData>();
        _deflateSoundData = deflateSound.GetComponent<AudioData>();
        _hitBallSoundData = hitBallSound.GetComponent<AudioData>();
	}

    protected override void Update()
    {
        base.Update();
        if (_fly)
        {
            SwitchToAnimationState("floating",true);
            //Player.GetComponent<Rigidbody2D>().mass = 1;
            Player.GetComponent<Rigidbody2D>().gravityScale = 2;
            //CaracteristicsBodyPart.GripRangeCoef = 0.5F;
            CaracteristicsBodyPart.ForceJumpCoef = -0.5F;
        }
        else
        {
            //Player.GetComponent<Rigidbody2D>().mass = 60;
            Player.GetComponent<Rigidbody2D>().gravityScale = 7;
            //CaracteristicsBodyPart.GripRangeCoef = 0.0F;
            CaracteristicsBodyPart.ForceJumpCoef = -0.0F;
        }
    }

    public override bool CanUse()
    {
        return true;
    }

    public override void Use()
    {
        if (state != States.startAction && state != States.loopAction && state != States.endAction)
        {
            if (Player.OwnedBall != null)
            {
                Vector2 vector = this.GetHitDirection();

                Debug.Log("launch ball in direction : " + vector);
                Player.OwnedBall.HitBy(Player, this.GetHitDirection(), Force);
            }
            Debug.Log("Start Action");
            state = States.startAction;
            Player.disableBallCatcher(0.05f);

            _fly = true;
            SwitchToAnimationState("inflate", true);
            AudioManager.Instance.Play(_inflateSoundData);
        }
    }

    public void StopUse()
    {
        _fly = false;
        SwitchToAnimationState("deflate",true);
        AudioManager.Instance.Play(_deflateSoundData);
    }

    public override void Action()
    {
        Debug.Log("JellyFish ACTION");
        foreach (Ball b in BallsManager.Instance.Balls)
        {
            if (b.GetComponent<Collider2D>().bounds.Intersects(this.GetComponent<Collider2D>().bounds))
            {
                _hitResolver = b.GetComponent<HitResolverBall>();
                if (_hitResolver != null)
                {
                    _hitResolver.Resolve((b.transform.position - transform.position).normalized, Force, Player);
                    //b.GetComponent<Rigidbody2D>().AddForce(new Vector2(10000,0), ForceMode2D.Impulse);
                    AudioManager.Instance.Play(_hitBallSoundData);
                }
            }
        }
        foreach (HumanPLayer hp in HumanPlayersManager.Instance.HumanPLayers)
        {
            if (hp.Player.GetComponent<Collider2D>().bounds.Intersects(this.GetComponent<Collider2D>().bounds))
            {
                _hitResolver = hp.Player.GetComponent<HitResolverPlayer>();
                if (_hitResolver != null && !PlayerTriggered.ContainsKey(hp.Player))
                {
                    Debug.Log("Trouver quelqu'un a taper");
                    _hitResolver.Resolve((hp.Player.transform.position - transform.position).normalized, Force, Player);
                    // hp.Player.GetComponent<Rigidbody2D>().AddForce(new Vector2(10000,0), ForceMode2D.Force);
                    PlayerTriggered.Add(hp.Player, true);
                    AudioManager.Instance.Play(_hitBallSoundData);
                }
            }
        }
    }

    public override Vector2 GetHitDirection()
    {
        return base.GetHitDirection();
    }

    void OnDestroy ()
    {
        InputManager.Instance.Remove(_deflateAction);
    }
}
