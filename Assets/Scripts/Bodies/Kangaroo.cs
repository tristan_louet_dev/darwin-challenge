﻿using UnityEngine;
using Audio;

public class Kangaroo : BodyPart
{
    public GameObject kickSound;

    private AudioData _kickSoundData;

    private bool wasJumpingUp = false;
    private bool wasFalling = false;

	// Use this for initialization
    protected override void Start () {
	    base.Start();
        _kickSoundData = kickSound.GetComponent<AudioData>();
	}

    protected override void Update()
    {
        base.Update();

        if (state == States.iddle)
        {
            var rigidbody = Player.GetComponent<Rigidbody2D>();
            if (!wasJumpingUp && rigidbody.velocity.y > 0 && !Player.OnGround)
            {
                wasJumpingUp = true;
                SwitchToAnimationState("jumpAscending", true);
            }
            else if (!wasFalling && rigidbody.velocity.y < 0 && !Player.OnGround)
            {
                wasFalling = true;
                SwitchToAnimationState("jumpDescending", true);
            }
            if (Player.OnGround)
            {
                wasFalling = false;
                wasJumpingUp = false;
                if (Mathf.Abs(rigidbody.velocity.x) > 0.25f)
                {
                    if (!isAction)
                        SwitchToAnimationState("run", true);
                }
                else
                {
                    if (!isAction)
                        SwitchToAnimationState("idle", true);
                }
            }
        }
    }

    public override bool CanUse()
    {
        return !isInCoolDown();
    }

    public override void Use()
    {
        if (CanUse() && state != States.startAction && state != States.loopAction && state != States.endAction)
        {
            base.Use();
            SwitchToAnimationState("shoot", true);
            AudioManager.Instance.Play(_kickSoundData);
        }
    }

    public override void Action()
    {
        base.Action();
    }

    public override Vector2 GetHitDirection()
    {
        return base.GetHitDirection();
    }
}
