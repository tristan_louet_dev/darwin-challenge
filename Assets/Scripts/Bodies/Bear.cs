﻿using Audio;
using UnityEngine;

public class Bear : BodyPart
{
    public GameObject punchSound;

    private AudioData _punchSoundData;

    protected override void Start() {
        _punchSoundData = punchSound.GetComponent<AudioData>();
        base.Start();
    }

    public override bool CanUse()
    {
        return (!this.isInCoolDown());
    }

    public override void Use()
    {
        if (CanUse() && state != States.startAction && state != States.loopAction && state != States.endAction)
        {
            base.Use();
            SwitchToAnimationState("punch", false);
            print("playsound");
            AudioManager.Instance.Play(_punchSoundData);
        }
    }

    public override void Action()
    {
       base.Action();
    }

    public override Vector2 GetHitDirection()
    {
        return base.GetHitDirection();
    }
}
