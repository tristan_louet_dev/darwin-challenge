﻿using UnityEngine;
using System.Collections;

public class ChameleonHeadController : MonoBehaviour
{

    public  GameObject Tongue;
    private ChameleonTongueController _chameleonTongueController;

	// Use this for initialization
    void Start () {
        _chameleonTongueController = Tongue.GetComponent<ChameleonTongueController>();
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject == Tongue)
        {
            if (_chameleonTongueController.state == ChameleonTongueController.TongueState.reload || _chameleonTongueController.state == ChameleonTongueController.TongueState.gripped)
            {
                _chameleonTongueController.Arrived();
            }
        }
    }
}
