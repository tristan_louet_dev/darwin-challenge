﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Entities;

public class HitResolverBall : HitResolver {

    public override void Resolve(Vector2 direction, float force, Player p)
    {
        //Debug.Log("ball hit");
		//this.GetComponent<Ball>().HitBy(p.gameObject, direction, force);
        //Debug.Log("_rigidbody.velocity " + _rigidbody.velocity);

        //transform.position += (Vector3)direction.normalized * 0.5f; //euh.. pourquoi on tp la ball ?

        _rigidbody.velocity = Vector2.zero;//direction * _rigidbody.velocity.magnitude;
        this.GetComponent<Rigidbody2D>().AddForce(direction*force);

        Debug.Log("Player " + p + ", direction " + direction + ", force " + force);
        //gameObject.GetComponent<Ball>().HitBy(p, direction, force);

    }
}
