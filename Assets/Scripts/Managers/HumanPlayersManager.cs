﻿using System.Collections.Generic;
using Assets.Scripts.Entities;
using Assets.Utils;
using UnityEngine;
using Utils;

namespace Assets.Scripts.Managers
{
    public class HumanPlayersManager : Singleton<HumanPlayersManager>
    {
        [SerializeField]
        private GameObject _characterPrefab = null;

        public string LevelName = "level";

        public List<HumanPLayer> HumanPLayers { get; set; }
        private Team _nextPlayerTeam = Team.None;

        private int _nextPlayerId;

        public override void Awake()
        {
            base.Awake();
            _nextPlayerId = 0;
            _nextPlayerTeam = Team.Blue;
            HumanPLayers = new List<HumanPLayer>();
            DontDestroyOnLoad(gameObject);

            print("number of xpads detected : " + InputManager.Instance.X360GamePads.Count);
        }

        public void InitialisePlayersForSelectionScreen()
        {
            destroyPlayers();
            _nextPlayerId = 0;
            HumanPLayers = new List<HumanPLayer>();
            for (int i = 0; i < InputManager.Instance.X360GamePads.Count; ++i)
            {
                var xpad = InputManager.Instance.X360GamePads[i];
                Debug.Log("xpad : " + xpad);
                //xpad.SetAllDeadZones(0.2f);

                CharacterSelectionManager.Instance.addHumanPlayer(xpad, _nextPlayerId);
                _nextPlayerId++;
            }
        }

        public void instantiatePlayers(int playerId, bool blueTeam, X360GamePad xpad, GameObject head, GameObject chest, GameObject legs)
        {
            Player p = Instantiate(_characterPrefab).GetComponent<Player>();
            p.SetBodies(head, chest, legs);

            if (p != null)
            {
                DontDestroyOnLoad(p.gameObject);
                p.Team = blueTeam ? Team.Blue : Team.Red;
                var hP = new HumanPLayer(xpad, p, playerId);
                HumanPLayers.Add(hP);
            }
        }

        public void removeBodiesActions()
        {
            foreach (HumanPLayer hp in HumanPLayers)
            {
                hp.Player.removeActions();
            }
        }

        public void destroyPlayers()
        {
            foreach(HumanPLayer hp in HumanPLayers)
            {
                hp.Player.removeActions();
                Destroy(hp.Player.gameObject);
            }
            HumanPLayers.Clear();
        }

        void OnDestroy()
        {
            //destroyPlayers();
        }
    }
}
