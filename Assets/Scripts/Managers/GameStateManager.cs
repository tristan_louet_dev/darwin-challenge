﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    public class GameStateManager : Singleton<GameStateManager>
    {
        public enum GameState
        {
            defaultState = 0,
            game,
            selection,
            review,
            mainMenu,
            option,
            credits,
            starter
        };

        [SerializeField]
        private GameState _currentGameState;
        public GameState currentGameState 
        {
            get { return _currentGameState ; }
            private set { _currentGameState = currentGameState ;}
        }

        // struc that will be convert in dictionnary
        [System.Serializable]
        public struct LevelState 
        {
            public string LevelName;
            public GameState linkedState;
        }

        [SerializeField]
        public LevelState[] levelAndState;

        private Dictionary<string, GameState> _levelbasedState;

        void Start()
        {
            DontDestroyOnLoad(this.gameObject);
            if(levelAndState == null)
            {
                levelAndState = new LevelState[0];
            }

            _levelbasedState = new Dictionary<string, GameState>();
            fillDictonnary();
        }
        
        private void fillDictonnary()
        {
            foreach(LevelState lvlState in levelAndState)
            {
                _levelbasedState.Add(lvlState.LevelName, lvlState.linkedState);
            }
        }

        void OnLevelWasLoaded(int loadedLevel)
        {
            if(_levelbasedState.ContainsKey(Application.loadedLevelName))
            {
                _currentGameState = _levelbasedState[Application.loadedLevelName];
            }
            else
            {
                _currentGameState = GameState.defaultState;
                Debug.LogError("current scene is not recognized by game state manager, switching to default state. Fix it as soon as you can");
            }
        }
    }
}
