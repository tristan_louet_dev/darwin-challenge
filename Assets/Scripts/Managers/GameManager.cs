﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Managers;
using Assets.Scripts.Managers.Events;
using Assets.Scripts.Entities;
using Assets.Utils;
using Audio;

public class GameManager : Singleton<GameManager>
{
    [SerializeField]
    private int _winScore = 5;
    public int WinScore { get { return _winScore; } }

    [SerializeField]
    private float _gameDuration = 10.0F; // en minutes
    public float GameDuration { get { return _gameDuration; } }

    public override void Awake()
    {
        DontDestroyOnLoad(gameObject);
        base.Awake();
        EventManager.OnGoal += (p, b, g, t) =>
        {
            CheckGame(t);
        };
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (Time.timeSinceLevelLoad / 60.0 >= _gameDuration)
        {
            EndGame();
        }

        // end game shortcut, debug
        if(Input.GetAxis("EndGame") != 0.0f)
        {
            EndGame();
        }
	}

    private void CheckGame(Team t)
    {
        if (ScoreManager.Instance[t].Value >= _winScore)
        {
            EndGame();
        }
    }

    public void EndGame()
    {
        EventManager.OnGoal.RemoveAllDelegate();
        EventManager.OnGameStarting.RemoveAllDelegate();
        EventManager.OnNewBallSpawned.RemoveAllDelegate();
        EventManager.OnNextBallTimerTick.RemoveAllDelegate();

        _gameDuration = Time.timeSinceLevelLoad; // for reviewScreen
        AudioManager.Instance.StopAllAndClear();

        AutoFade.LoadLevel("reviewScreen", 0.5f, 0.1f, Color.black); // launch level "level"
        Debug.Log("FIN !!!!!!!!!!!!!!!!");
    }
}
