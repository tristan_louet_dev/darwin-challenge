﻿using System.Collections.Generic;
using Assets.Scripts.Entities;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    public class ScoreManager : Singleton<ScoreManager>
    {
        public class Score
        {
            public uint Value { get; private set; }

            public void Add(uint value)
            {
                Value += value;
            }

            public static Score operator +(Score s, uint v)
            {
                s.Add(v);
                return s;
            }

            public Score()
            {
                Value = 0;
            }
        }

        private Dictionary<Team, Score> _teamScores;

        public Score this[Team t]
        {
            get { return _teamScores.ContainsKey(t) ? _teamScores[t] : null; }
            set
            {
                _teamScores[t] = value;
                Debug.Log("Team " + t + "'s score is now " + this[t].Value);
            }
        }

        public override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(gameObject);
            _teamScores = new Dictionary<Team, Score>();
            foreach (var team in TeamExtensions.Values())
            {
                _teamScores.Add(team, new Score());
            }
        }
    }
}
