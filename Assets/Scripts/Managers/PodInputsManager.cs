﻿using Assets.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Utils;
using Action = Utils.Action;

namespace Assets.Scripts.Managers
{
    class PodInputsManager : MonoBehaviour
    {
        public float moveDelayDuration = 0.25f; // time to wait between two movements
        private float _canMoveTime = 0.0f;

        private PodScript _pod;
        private bool _hasXpad = false;
        private X360GamePad _xpad;
        private int _playerId;

        private ContextualAction _startAction;
        private ContextualAction _aAction;
        private ContextualAction _bAction;
        private ContextualAction _yAction;

        void Start()
        {
            _pod = GetComponent<PodScript>();
        }

        void Update()
        {
            if (_hasXpad && (_pod.selectionState == PodScript.SelectionState.modify) && Time.time >= _canMoveTime)
            {
                if (_xpad.LeftJoystickVertical.Value != 0.0f)
                {
                    _pod.changeZone(_xpad.LeftJoystickVertical.Value < 0.0f);
                    _canMoveTime = Time.time + moveDelayDuration;
                }

                if (_xpad.LeftJoystickHorizontal.Value != 0.0f)
                {
                    _pod.changeBodyPart(_xpad.LeftJoystickHorizontal.Value < 0.0f);
                    _canMoveTime = Time.time + moveDelayDuration;
                }
            }
        }

        public void setPodXpad(X360GamePad xpad, int playerId)
        {
            _xpad = xpad;
            _hasXpad = true;
            _playerId = playerId;

            xpad.SetAllDeadZones(0.5f);

            _startAction = new ContextualAction(xpad.Home, GameStateManager.GameState.selection);
            _startAction.Callback = () =>
            {
                if (Application.loadedLevelName == "characterSelection")
                {
                    switch (_pod.selectionState)
                    {
                        case PodScript.SelectionState.wait:
                            _pod.OpenDoor();
                            if (CharacterSelectionManager.Instance.areAllPlayersReady())
                            {
                                CharacterSelectionManager.Instance.checkReadyPlayers();
                            }
                            break;

                        case PodScript.SelectionState.modify:
                            _pod.SetPlayerReady();
                            break;

                        case PodScript.SelectionState.ready:
                            //CharacterSelectionManager.Instance.checkReadyPlayers();
                            if (CharacterSelectionManager.Instance.areAllPlayersReady())
                            {
                                CharacterSelectionManager.Instance.LaunchGame();
                            }
                            break;

                        default:
                            break;
                    }
                }
                else
                {
                    removeActions();
                }
            };
            InputManager.Instance.Add(_startAction);

            _aAction = new ContextualAction(xpad.A, GameStateManager.GameState.selection);
            _aAction.Callback = () =>
            {
                if (Application.loadedLevelName == "characterSelection")
                {
                    switch (_pod.selectionState)
                    {
                        case PodScript.SelectionState.wait:
                            _pod.OpenDoor();
                            break;

                        case PodScript.SelectionState.modify:
                            _pod.SetPlayerReady();
                            break;

                        default:
                            break;
                    }
                }
                else
                {
                    removeActions();
                }
            };
            InputManager.Instance.Add(_aAction);

            _bAction = new ContextualAction(xpad.B, GameStateManager.GameState.selection);
            _bAction.Callback = () =>
            {
                if (Application.loadedLevelName == "characterSelection")
                {
                    switch (_pod.selectionState)
                    {
                        case PodScript.SelectionState.wait:
                            if (CharacterSelectionManager.Instance.areAllPlayerWaiting())
                            {
                                CharacterSelectionManager.Instance.backToMainMenu();
                            }
                            break;

                        case PodScript.SelectionState.modify:
                            _pod.CloseDoor();
                            CharacterSelectionManager.Instance.checkReadyPlayers();
                            break;

                        case PodScript.SelectionState.ready:
                            _pod.SetPlayerNotReady();
                            break;

                        default:
                            break;
                    }
                }
                else
                {
                    removeActions();
                }
            };
            InputManager.Instance.Add(_bAction);

            _yAction = new ContextualAction(xpad.Y, GameStateManager.GameState.selection);
            _yAction.Callback = () =>
            {
                if (Application.loadedLevelName == "characterSelection")
                {
                    switch (_pod.selectionState)
                    {
                        case PodScript.SelectionState.wait:
                            break;

                        case PodScript.SelectionState.modify:
                            _pod.RandomCharacter();
                            break;

                        default:
                            break;
                    }
                }
                else
                {
                    removeActions();
                }
            };
            InputManager.Instance.Add(_yAction);
        }

        public void InstatiatePlayer()
        {
            if(_pod.selectionState == PodScript.SelectionState.ready)
            {
                removeActions();
                HumanPlayersManager.Instance.instantiatePlayers(_playerId, _pod.blueTeam, _xpad,
                                                                    _pod.getBodyPartGamePrefab(PodScript.BodyPart.head),
                                                                    _pod.getBodyPartGamePrefab(PodScript.BodyPart.chest),
                                                                    _pod.getBodyPartGamePrefab(PodScript.BodyPart.legs));
            }
        }

        public bool hasXpad()
        {
            return _hasXpad;
        }

        public void removeActions()
        {
            InputManager.Instance.Remove(_startAction);
            InputManager.Instance.Remove(_aAction);
            InputManager.Instance.Remove(_bAction);
            InputManager.Instance.Remove(_yAction);
        }
    }
}
