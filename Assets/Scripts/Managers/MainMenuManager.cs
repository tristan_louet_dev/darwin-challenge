﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Utils;

namespace Assets.Scripts.Managers
{
    class MainMenuManager : Singleton<MainMenuManager>
    {
        public float moveDelayDuration = 0.2f; // time to wait between two movements
        private float _canMoveTime = 0.0f;

        [SerializeField]
        private MainMenu _mainMenuScript = null;

        private List<X360GamePad> _xpads;

        private ContextualAction _startAction;
        private ContextualAction _aAction;

        public override void Awake ()
        {
            base.Awake();

            _xpads = new List<X360GamePad>();

            foreach(X360GamePad xpad in InputManager.Instance.X360GamePads)
            {
                addXpad(xpad);
            }
        }

        void Update()
        {
            if (Time.time >= _canMoveTime)
            {
                foreach(X360GamePad xpad in _xpads)
                {
                    if(xpad.LeftJoystickVertical.Value < 0.0f || xpad.LeftJoystickHorizontal.Value < 0.0f)
                    {
                        _mainMenuScript.Next();
                        _canMoveTime = Time.time + moveDelayDuration;
                    }

                    if(xpad.LeftJoystickVertical.Value > 0.0f || xpad.LeftJoystickHorizontal.Value > 0.0f)
                    {
                        _mainMenuScript.Previous();
                        _canMoveTime = Time.time + moveDelayDuration;
                    }
                }
            }
        }

        void OnDestroy()
        {
            removeActions();
        }

        public void addXpad(X360GamePad xpad)
        {
            _startAction = new ContextualAction(xpad.Home, GameStateManager.GameState.mainMenu);
            _startAction.Callback = () =>
            {
                if (Application.loadedLevelName == "mainMenu")
                {
                    _mainMenuScript.Click();
                }
                else
                {
                    removeActions();
                }
                
            };
            InputManager.Instance.Add(_startAction);

            _aAction = new ContextualAction(xpad.A, GameStateManager.GameState.mainMenu);
            _aAction.Callback = () =>
            {
                if (Application.loadedLevelName == "mainMenu")
                {
                    _mainMenuScript.Click();
                }
                else
                {
                    removeActions();
                }
            };
            InputManager.Instance.Add(_aAction);

            _xpads.Add(xpad);
        }

        private void removeActions()
        {
            InputManager.Instance.Remove(_startAction);
            InputManager.Instance.Remove(_aAction);
        }
    }
}
