﻿using Assets.Scripts.Entities;
using Assets.Scripts.Managers;
using Assets.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using UnityEngine;
using Utils;
using Action = Utils.Action;

namespace Assets.Scripts.Managers
{
    class CreditInputManager : MonoBehaviour
    {
        #region Static fields

        private static CreditInputManager _instance;

        public static CreditInputManager Instance
        {
            get { return _instance ?? (_instance = FindObjectOfType<CreditInputManager>()); }
        }

        #endregion

        private List<X360GamePad> _xpads;

        private ContextualAction _startAction;
        private ContextualAction _aAction;
        private ContextualAction _bAction;

        void Awake()
        {
            _xpads = new List<X360GamePad>();

            foreach (X360GamePad xpad in InputManager.Instance.X360GamePads)
            {
                addXpad(xpad);
            }
        }

        void OnDestroy()
        {
            removeActions();
        }

        public void addXpad(X360GamePad xpad)
        {
            _startAction = new ContextualAction(xpad.Home, GameStateManager.GameState.credits);
            _startAction.Callback = () =>
            {
                creditAction();
            };
            InputManager.Instance.Add(_startAction);

            _aAction = new ContextualAction(xpad.A, GameStateManager.GameState.credits);
            _aAction.Callback = () =>
            {
                creditAction();
            };
            InputManager.Instance.Add(_aAction);

            _bAction = new ContextualAction(xpad.B, GameStateManager.GameState.credits);
            _bAction.Callback = () =>
            {
                creditAction();
            };

            InputManager.Instance.Add(_bAction);

            _xpads.Add(xpad);
        }

        private void removeActions()
        {
            InputManager.Instance.Remove(_startAction);
            InputManager.Instance.Remove(_aAction);
            InputManager.Instance.Remove(_bAction);
        }

        private void creditAction()
        {
            if (Application.loadedLevelName == "credits")
            {
                AutoFade.LoadLevel("mainmenu", 0.5f, 0.5f, Color.black);
                removeActions();
            }
            else
            {
                removeActions();
            }
        }
    }
}
