﻿using System.Collections.Generic;
using Assets.Utils;
using UnityEngine;
using Utils;
using Audio;

namespace Assets.Scripts.Managers
{
    class CharacterSelectionManager : Singleton<CharacterSelectionManager>
    {
        public List<GameObject> pods; // { get; set; }

        public GameObject startToPlayFeedback;
        public GameObject launchGameSound;

        public string levelToLoad = "shorterLevel";

        private bool _allPlayerReady;

        private Animator _startToPlayAnimator;
        private AudioData _startToPlayAudioData;

        private int _nextPodId = 0;
        
        void Start ()
        {
            pods = pods ?? new List<GameObject>();
            startToPlayFeedback = startToPlayFeedback ?? new GameObject();
            launchGameSound = launchGameSound ?? new GameObject();
        }

        void OnLevelWasLoaded(int level)
        {
            // note : for now, selection level is 1 on build setting, maight change (meaby securize this?)
            //if(level == 1)
            //{
                init();
            //}
        }

        public void addHumanPlayer(X360GamePad xpad, int playerId)
        {
            if (playerId < pods.Count && _nextPodId < pods.Count)
            {
                print(name + " set gamepad " + xpad.Name + " to pod " + _nextPodId);
                pods[_nextPodId].GetComponent<PodInputsManager>().setPodXpad(xpad, playerId);
                _nextPodId++;
            }

            displayPodsState();
        }

        public void LaunchGame()
        {
            foreach (GameObject pod in pods)
            {
                pod.GetComponent<PodInputsManager>().InstatiatePlayer();
                pod.GetComponent<PodScript>().scientist.GetComponent<Animator>().SetTrigger("write");
            }
            AudioManager.Instance.StopAllAndClear();
            AudioManager.Instance.Play(launchGameSound.GetComponent<AudioData>());
            AutoFade.LoadLevel(levelToLoad, 2.5f, 0.5f, Color.black); // launch level
        }

        public void backToMainMenu()
        {
            foreach (GameObject pod in pods)
            {

                pod.GetComponent<PodScript>().scientist.GetComponent<Animator>().SetTrigger("write");
            }
            AudioManager.Instance.StopAllAndClear();
            AudioManager.Instance.Play(launchGameSound.GetComponent<AudioData>());
            AutoFade.LoadLevel("main_menu", 2.5f, 0.5f, Color.black); // launch level main_menu
        }

        public bool areAllPlayerWaiting()
        {
            foreach(GameObject pod in pods)
            {
                if(pod.GetComponent<PodScript>().selectionState != PodScript.SelectionState.wait)
                {
                    return false;
                }
            }
            return true;
        }

        public bool areAllPlayersReady()
        {
            return _allPlayerReady;
        }

        // only checks if all players are ready
        // a player is considered not ready if he is modifying its character
        // if a plyer does not have open its pod, he is considered as ready (but he won't spawn in play
        public void checkReadyPlayers()
        {
            bool allPlayerWaiting = true;
            foreach (GameObject pod in pods)
            {
                if (pod.GetComponent<PodInputsManager>().hasXpad())
                {
                    if (pod.GetComponent<PodScript>().selectionState == PodScript.SelectionState.modify)
                    {
                        if (_allPlayerReady)
                        {
                            _startToPlayAnimator.SetTrigger("closing");
                        }
                        _allPlayerReady = false;
                        return;
                    }
                    else if (pod.GetComponent<PodScript>().selectionState != PodScript.SelectionState.wait)
                    {
                        allPlayerWaiting = false;
                    }
                }
            }

            if (!allPlayerWaiting)
            {
                _allPlayerReady = true;
                _startToPlayAnimator.SetTrigger("openning");
                AudioManager.Instance.Play(_startToPlayAudioData);
            }
        }

        private void init()
        {
            print("initializing " + name);
            _nextPodId = 0;
            _startToPlayAnimator = startToPlayFeedback.GetComponent<Animator>();
            _startToPlayAudioData = startToPlayFeedback.GetComponent<AudioData>();
            HumanPlayersManager.Instance.InitialisePlayersForSelectionScreen();
        }

        private void displayPodsState()
        {
            print("DISPLAYING PODS STATE");
            foreach(GameObject p in pods)
            {
                print("     " + p.name + " does" + (p.GetComponent<PodInputsManager>().hasXpad() ? "" : " not") + " have xpad");
            }
        }
    }
}
