﻿using Assets.Scripts.Entities;
using Assets.Scripts.Entities.Spawners;

namespace Assets.Scripts.Managers.Events
{
    /// <summary>
    /// A class centralizing game events.
    /// <remarks>This class is static and cannot be instanciated.</remarks>
    /// </summary>
    public static class EventManager
    {
        public static Event OnGameStarting = new Event();
        /// <summary>
        /// Fired when a goal occurs. The Player put the Ball in the Goal and gives a point to the Team.
        /// </summary>
        public static Event<Player, Ball, Goal, Team> OnGoal = new Event<Player, Ball, Goal, Team>(); 

        public static Event<int> OnNextBallTimerTick = new Event<int>();
        public static Event<Ball, BallsSpawner> OnNewBallSpawned = new Event<Ball, BallsSpawner>();

    }
}