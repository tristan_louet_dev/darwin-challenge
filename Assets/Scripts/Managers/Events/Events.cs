﻿using System;

namespace Assets.Scripts.Managers.Events
{

    #region No parameter callbacks events

    /// <summary>
    /// A generic wrapper of C# events.
    /// </summary>
    public class Event
    {
        public delegate void Handler();

        private event Handler _event;
        private event Handler _eventLate;

        public static Event operator +(Event e, Handler h)
        {
            e._event += h;
            return e;
        }

        public void RegisterLate(Handler h)
        {
            this._eventLate += h;
        }

        public static Event operator -(Event e, Handler h)
        {
            e._event -= h;
            return e;
        }

        public void Fire()
        {
            if (_event != null)
            {
                _event();
            }

            if (_eventLate != null)
            {
                _eventLate();
            }
        }

        public void RemoveAllDelegate()
        {
            if(_event != null)
            {
                Delegate[] clientList = _event.GetInvocationList();
                foreach(var d in clientList)
                {
                    _event -= (d as Handler);
                }
            }
        }

    }

    #endregion

    #region One parameter callbacks events

    public class Event<T1>
    {
        public delegate void Handler(T1 element);

        private event Handler _event;
        private event Handler _eventLate;

        public static Event<T1> operator +(Event<T1> e, Handler h)
        {
            e._event += h;
            return e;
        }

        public void RegisterLate(Handler h)
        {
            this._eventLate += h;
        }

        public static Event<T1> operator -(Event<T1> e, Handler h)
        {
            e._event -= h;
            return e;
        }

        public void Fire(T1 element)
        {
            if (_event != null)
            {
                _event(element);
            }

            if (_eventLate != null)
            {
                _eventLate(element);
            }
        }

        public void RemoveAllDelegate()
        {
            if (_event != null)
            {
                Delegate[] clientList = _event.GetInvocationList();
                foreach (var d in clientList)
                {
                    _event -= (d as Handler);
                }
            }
        }
    }

    #endregion

    #region Two parameters callbacks events

    public class Event<T1, T2>
    {
        public delegate void Handler(T1 element1, T2 element2);

        private event Handler _event;
        private event Handler _eventLate;

        public static Event<T1, T2> operator +(Event<T1, T2> e, Handler h)
        {
            e._event += h;
            return e;
        }

        public void RegisterLate(Handler h)
        {
            this._eventLate += h;
        }

        public static Event<T1, T2> operator -(Event<T1, T2> e, Handler h)
        {
            e._event -= h;
            return e;
        }

        public void Fire(T1 element1, T2 element2)
        {
            if (_event != null)
            {
                _event(element1, element2);
            }

            if (_eventLate != null)
            {
                _eventLate(element1, element2);
            }
        }

        public void RemoveAllDelegate()
        {
            if (_event != null)
            {
                Delegate[] clientList = _event.GetInvocationList();
                foreach (var d in clientList)
                {
                    _event -= (d as Handler);
                }
            }
        }
    }

    #endregion

    #region Three parameters callbacks events

    public class Event<T1, T2, T3>
    {
        public delegate void Handler(T1 element1, T2 element2, T3 element3);

        private event Handler _event;
        private event Handler _eventLate;

        public static Event<T1, T2, T3> operator +(Event<T1, T2, T3> e, Handler h)
        {
            e._event += h;
            return e;
        }

        public void RegisterLate(Handler h)
        {
            this._eventLate += h;
        }

        public static Event<T1, T2, T3> operator -(Event<T1, T2, T3> e, Handler h)
        {
            e._event -= h;
            return e;
        }

        public void Fire(T1 element1, T2 element2, T3 element3)
        {
            if (_event != null)
            {
                _event(element1, element2, element3);
            }

            if (_eventLate != null)
            {
                _eventLate(element1, element2, element3);
            }
        }

        public void RemoveAllDelegate()
        {
            if (_event != null)
            {
                Delegate[] clientList = _event.GetInvocationList();
                foreach (var d in clientList)
                {
                    _event -= (d as Handler);
                }
            }
        }
    }

    #endregion

    #region Four parameters callbacks events

    public class Event<T1, T2, T3, T4>
    {
        public delegate void Handler(T1 element1, T2 element2, T3 element3, T4 element4);

        private event Handler _event;
        private event Handler _eventLate;

        public static Event<T1, T2, T3, T4> operator +(Event<T1, T2, T3, T4> e, Handler h)
        {
            e._event += h;
            return e;
        }

        public void RegisterLate(Handler h)
        {
            this._eventLate += h;
        }

        public static Event<T1, T2, T3, T4> operator -(Event<T1, T2, T3, T4> e, Handler h)
        {
            e._event -= h;
            return e;
        }

        public void Fire(T1 element1, T2 element2, T3 element3, T4 element4)
        {
            if (_event != null)
            {
                _event(element1, element2, element3, element4);
            }

            if (_eventLate != null)
            {
                _eventLate(element1, element2, element3, element4);
            }
        }

        public void RemoveAllDelegate()
        {
            if (_event != null)
            {
                Delegate[] clientList = _event.GetInvocationList();
                foreach (var d in clientList)
                {
                    _event -= (d as Handler);
                }
            }
        }
    }

    #endregion
}
