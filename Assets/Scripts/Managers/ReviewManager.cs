﻿using Assets.Scripts.Entities;
using Assets.Scripts.ReviewScreen;
using Assets.Utils;
using Audio;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Utils;
using Action = Utils.Action;

namespace Assets.Scripts.Managers
{
    class ReviewManager : MonoBehaviour
    {
        public GameObject experimentNumberText = null;
        public GameObject timeValueText = null;
        public GameObject blueTeamScoreText = null;
        public GameObject yellowTeamScoreText = null;

        public GameObject[] reviewContainers = null;

        public int blueTeamScore = 0;
        public int yellowTeamScore = 0;

        public float matchSeconds = 0.0f; // match time in seconds

        public GameObject stampBlueWin = null;
        public GameObject stampYellowWin = null;
        public GameObject stampBlueLose = null;
        public GameObject stampYellowLose = null;
        public GameObject stampEquality = null;

        private ContextualAction _quitActionStart;
        private ContextualAction _quitActionA;
        private ContextualAction _quitActionB;

        private List<X360GamePad> _xpads;

        void Start()
        {
            _xpads = new List<X360GamePad>();
            SetReviewScreen();
            AudioManager.Instance.StopAllAndClear();
        }

        void OnDestroy()
        {
            RemoveQuitActions();
        }

        public void SetReviewScreen()
        {
            print("SET REVIEW SCREEN");

            if(ScoreManager.Instance)
            {
                blueTeamScore = (int)ScoreManager.Instance[Team.Blue].Value;
                yellowTeamScore = (int)ScoreManager.Instance[Team.Red].Value;
            }

            if(GameManager.Instance)
            {
                matchSeconds = GameManager.Instance.GameDuration;
            }
            
            experimentNumberText.GetComponent<Text>().text = Random.Range(100000, 999999).ToString();
            timeValueText.GetComponent<Text>().text = parseTimeText();

            blueTeamScoreText.GetComponent<Text>().fontSize += blueTeamScore;
            blueTeamScoreText.GetComponent<Text>().text = blueTeamScore.ToString();

            yellowTeamScoreText.GetComponent<Text>().fontSize += yellowTeamScore;
            yellowTeamScoreText.GetComponent<Text>().text = yellowTeamScore.ToString();

            if(blueTeamScore > yellowTeamScore)
            {
                stampBlueWin.SetActive(true);
                stampYellowLose.SetActive(true);
            }
            else if (blueTeamScore < yellowTeamScore)
            {
                stampYellowWin.SetActive(true);
                stampBlueLose.SetActive(true);
            }
            else
            {
                stampEquality.SetActive(true);
            }

            int humanPlayerNumber = Mathf.Min (HumanPlayersManager.Instance.HumanPLayers.Count, reviewContainers.Length);
            print(humanPlayerNumber);
            int bestScore = 0;
            int bestPlayersIndex = 0;

            for(int i = 0; i < humanPlayerNumber; i++)
            {
                reviewContainers[i].SetActive(true);

                ReviewCharacterScript review = reviewContainers[i].GetComponent<ReviewCharacterScript>();
                Player player = HumanPlayersManager.Instance.HumanPLayers[i].Player;

                if(player.GetGoals > bestScore)
                {
                    bestScore = player.GetGoals;
                    bestPlayersIndex = i;
                }
                review.SetReviewCharacterInformations(player.GetGoals, 0, player.Team == Team.Blue);
                if(player.Team == Team.Blue)
                {
                    review.SetReviewCharacterSprites(player.Head.highSpriteBlue, player.Chest.highSpriteBlue, player.Legs.highSpriteBlue);
                }
                else
                {
                    review.SetReviewCharacterSprites(player.Head.highSpriteYellow, player.Chest.highSpriteYellow, player.Legs.highSpriteYellow);
                }

                addQuitAction(player.xpad);
            }

            reviewContainers[bestPlayersIndex].GetComponent<ReviewCharacterScript>().SetReviewCharacterBestPlayer();
        }

        // returns a string in form of mm : ss
        public string parseTimeText()
        {
            int minutes = ((int)matchSeconds) / 60;
            int seconds = ((int)matchSeconds) % 60;
            
            return (minutes < 10 ? "0" : "") + minutes.ToString() + " : " + (seconds < 10 ? "0" : "") + seconds.ToString(); 
        }

        private void addQuitAction (X360GamePad xpad)
        {
            print("add action for " + xpad.Name);

            _xpads.Add(xpad);

            _quitActionStart = new ContextualAction(xpad.Home, GameStateManager.GameState.review);
            _quitActionStart.Callback = () =>
            {
                QuitAction();                
            };
            InputManager.Instance.Add(_quitActionStart);

            _quitActionA = new ContextualAction(xpad.A, GameStateManager.GameState.review);
            _quitActionA.Callback = () =>
            {
                QuitAction();
            };
            InputManager.Instance.Add(_quitActionA);

            _quitActionB = new ContextualAction(xpad.B, GameStateManager.GameState.review);
            _quitActionB.Callback = () =>
            {
                QuitAction();
            };
            InputManager.Instance.Add(_quitActionB);
        }

        private void QuitAction()
        {
            if (Application.loadedLevelName == "reviewScreen")
            {
                if (ScoreManager.Instance)
                {
                    Destroy(ScoreManager.Instance.gameObject);
                }

                if (GameManager.Instance)
                {
                    Destroy(GameManager.Instance.gameObject);
                }

                AutoFade.LoadLevel("characterSelection", 1.5f, 0.5f, Color.black);
            }
            else
            {
                RemoveQuitActions();
            }
        }

        public void RemoveQuitActions()
        {
            InputManager.Instance.Remove(_quitActionStart);
            InputManager.Instance.Remove(_quitActionA);
            InputManager.Instance.Remove(_quitActionB);
        }
    }
}
