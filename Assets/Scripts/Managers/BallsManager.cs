﻿using System.Collections.Generic;
using Assets.Scripts.Entities;
namespace Assets.Scripts.Managers
{
    public class BallsManager : Singleton<BallsManager>
    {
        public List<Ball> Balls { get; private set; }

        public override void Awake()
        {
            //DontDestroyOnLoad(gameObject);
            Balls = new List<Ball>();
        }
    }
}
