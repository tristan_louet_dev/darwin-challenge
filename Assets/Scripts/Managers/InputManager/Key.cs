﻿using System.Linq;
using UnityEngine;

namespace Utils
{
    public class Key : IInput
    {
        private readonly KeyCode[] _keys;

        public Key(params KeyCode[] keys)
        {
            _keys = keys;
        }

        public bool IsActive()
        {
            return _keys.Any(Input.GetKey);
        }
    }
}