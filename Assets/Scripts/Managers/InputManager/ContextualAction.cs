﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Assets.Scripts.Managers
{
    public class ContextualAction : Action
    {
        private List<GameStateManager.GameState> _actionsStates;
        
        public ContextualAction(IInput input, params GameStateManager.GameState[] actionsStates) 
            : base (input)
        {
            Init(actionsStates);
        }
        
        public ContextualAction(IInput input, ActionType type = ActionType.Pressed, float delay = 0, 
                                params GameStateManager.GameState[] actionsStates) 
            : base (input, type, delay)
        {
            Init(actionsStates);
        }

        public ContextualAction(Axis axis, params GameStateManager.GameState[] actionsStates)
            : base(axis)
        {
            Init(actionsStates);
        }

        public ContextualAction(Axis axis, ActionType type = ActionType.Down, float delay = 0,
                                params GameStateManager.GameState[] actionsStates) 
            : base(axis, type, delay)
        {
            Init(actionsStates);
        }

        private void Init(params GameStateManager.GameState[] actionsStates)
        {
            _actionsStates = new List<GameStateManager.GameState>();

            if(!actionsStates.Any()) // if actionStates is empty
            {
                Debug.Log("<color=red>Action does not have states, swithching to default state to avoid errors, you should fix it as fast as you can</color>");
                _actionsStates.Add(GameStateManager.GameState.defaultState);
            }
            else
            {
                _actionsStates = actionsStates.ToList();
                Debug.Log(_actionsStates[0].ToString());
            }
        }

        public override void Fire ()
        {
            Debug.Log("prout");
            if(_actionsStates.Contains(GameStateManager.Instance.currentGameState))
            {
                base.Fire();
            }
            else
            {
                Debug.Log("action not fired because game is not in a good state. Current state is : "
                        + GameStateManager.Instance.currentGameState + " and action states are " + actionsToString());
            }
        }

        public override bool IsActive ()
        {
            if(_actionsStates.Contains(GameStateManager.Instance.currentGameState))
            {
                return base.IsActive();
            }
            return false;
        }

        public string actionsToString()
        {
            string returnString = "";

            foreach(GameStateManager.GameState gs in _actionsStates)
            {
                returnString += gs.ToString() + ", ";
            }
            return returnString;
        }
    }
}
