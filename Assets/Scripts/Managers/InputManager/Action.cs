﻿using UnityEngine;

namespace Utils
{
    public enum ActionType
    {
        Down,
        Hold,
        Pressed,
        Released
    }

    public class Action
    {
        #region Static fields

        public enum Operator
        {
            And,
            Or,
            Xor,
            Not
        }

        public static Action operator &(Action a1, Action a2)
        {
            return new Action(a1, a2, Operator.And);
        }

        public static Action operator |(Action a1, Action a2)
        {
            return new Action(a1, a2, Operator.Or);
        }

        public static Action operator ^(Action a1, Action a2)
        {
            return new Action(a1, a2, Operator.Xor);
        }

        public static Action operator !(Action a1)
        {
            return new Action(a1, null, Operator.Not);
        }

        #endregion

        public ActionType Type { get; set; }
        public bool IsAtomic { get; private set; }
        public float Delay { get; set; }

        private readonly Action _lhs;
        private readonly Action _rhs;
        private readonly IInput _input;
        private readonly Axis _axis;
        private readonly Operator _op;
        private bool _previousState;
        private float _lastTrigger;

        public delegate void ActionDelegate();

        public delegate void ActionAxisDelegate(float value);

        public ActionDelegate Callback;
        public ActionAxisDelegate AxisCallback;

        private Action(Action lhs, Action rhs, Operator op)
        {
            IsAtomic = false;
            _lhs = lhs;
            _rhs = rhs;
            _op = op;
            Delay = _lastTrigger = 0;
        }

        public Action(IInput input, ActionType type = ActionType.Pressed, float delay = 0)
        {
            IsAtomic = true;
            _input = input;
            _axis = null;
            Type = type;
            Delay = delay;
            _lastTrigger = - delay;
        }

        public Action(Axis axis, ActionType type = ActionType.Down, float delay = 0)
        {
            IsAtomic = true;
            _input = null;
            _axis = axis;
            Type = type;
            Delay = delay;
            _lastTrigger = -delay;
        }

        public virtual void Fire()
        {
            if (Callback != null)
                Callback();

            if (_axis == null) return;
            if (AxisCallback != null)
                AxisCallback(_axis.Value);
        }

        public virtual bool IsActive()
        {
            if (Time.time < _lastTrigger + Delay)
            {
                return false;
            }

            var state = false;

            if (IsAtomic)
            {
                var baseState = _input != null ? _input.IsActive() : _axis.IsActive();

                switch (Type)
                {
                    case ActionType.Down:
                        state = baseState;
                        break;

                    case ActionType.Pressed:
                        state = !_previousState && baseState;
                        break;

                    case ActionType.Released:
                        state = _previousState && !baseState;
                        break;

                    case ActionType.Hold:
                        state = _previousState && baseState;
                        break;
                }

                _previousState = baseState;
            }
            else
            {
                switch (_op)
                {
                    case Operator.And:
                        state = _lhs.IsActive() && _rhs.IsActive();
                        break;

                    case Operator.Or:
                        state = _lhs.IsActive() || _rhs.IsActive();
                        break;

                    case Operator.Xor:
                        state = _lhs.IsActive() ^ _rhs.IsActive();
                        break;

                    case Operator.Not:
                        state = !_lhs.IsActive();
                        break;
                }
            }

            if (state)
            {
                _lastTrigger = Time.time;
            }

            return state;
        }
    }
}