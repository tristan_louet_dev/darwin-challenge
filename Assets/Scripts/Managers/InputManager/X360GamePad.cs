﻿namespace Utils
{
    public class X360GamePad : GamePad
    {
        public Button A;
        public Button B;
        public Button X;
        public Button Y;

        public Button LB;
        public Button RB;

        public Button Back;
        public Button Home;

        public Button LeftAnalogButton;
        public Button RightAnalogButton;

        public Axis LT;
        public Axis RT;

        public Axis LeftJoystickHorizontal;
        public Axis LeftJoystickVertical;

        public Axis RightJoystickHorizontal;
        public Axis RightJoystickVertical;

        //public Axis DPadHorizontal;
        //public Axis DPadVertical;

        public X360GamePad(string name, int id) : base(name, id)
        {
            A = new Button(0, id);
            B = new Button(1, id);
            X = new Button(2, id);
            Y = new Button(3, id);

            LB = new Button(4, id);
            RB = new Button(5, id);

            Back = new Button(6, id);
            Home = new Button(7, id);

            LeftAnalogButton = new Button(8, id);
            RightAnalogButton = new Button(9, id);

            LT = new Axis("joystick " + id + " LT");
            RT = new Axis("joystick " + id + " RT");

            LeftJoystickHorizontal = new Axis("joystick " + id + " left horizontal", 0.1f);
            LeftJoystickVertical = new Axis("joystick " + id + " left vertical", 0.1f);

            RightJoystickHorizontal = new Axis("joystick " + id + " right horizontal", 0.1f);
            RightJoystickVertical = new Axis("joystick " + id + " right vertical", 0.1f);
        }
        public void SetAllDeadZones(float deadZone)
        {
            LT.DeadZone = deadZone;
            RT.DeadZone = deadZone;
            RightJoystickHorizontal.DeadZone = deadZone;
            RightJoystickVertical.DeadZone = deadZone;
            LeftJoystickHorizontal.DeadZone = deadZone;
            LeftJoystickVertical.DeadZone = deadZone;
        }
    }
}