﻿using UnityEngine;

namespace Utils
{
    public class Axis
    {
        public string Name { get; private set; }
        public float DeadZone { get; set; }

        public float Value
        {
            get
            {
                var val = Input.GetAxis(Name);
                return Mathf.Abs(val) < DeadZone ? 0 : val;
            }
        }

        public Axis(string name, float deadZone = 0.05f)
        {
            Name = name;
            DeadZone = deadZone;
        }

        public bool IsActive()
        {
            return true;
        }
    }

}