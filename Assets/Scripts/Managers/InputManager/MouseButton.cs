﻿
using System;
using System.Linq;
using UnityEngine;

namespace Utils
{
    public class MouseButton : IInput
    {
        public enum Button
        {
            Left = 0,
            Right = 1,
            Middle = 2
        }

        private readonly Button[] _buttons;

        public MouseButton(params Button[] buttons)
        {
            if (buttons.Length > 3)
            {
                throw new ArgumentException("Only 3 buttons at most can be provided", "buttons");
            }
            _buttons = buttons;
        }

        public bool IsActive()
        {
            return _buttons.Any(b => Input.GetMouseButton((int) b));
        }
    }
}
