﻿using UnityEngine;

namespace Utils
{
    public class GamePad
    {
        public class Button : IInput
        {
            private readonly string _name;

            public Button(int id, int controllerId)
            {
                _name = "joystick " + controllerId + " button " + id;
            }

            public bool IsActive()
            {
                return Input.GetButton(_name);
            }
        }

        public string Name { get; private set; }
        public int Id { get; private set; }
        protected string _id;

        public GamePad(string name, int id)
        {
            Name = name;
            Id = id;
            _id = id.ToString();
        }
    }
}
