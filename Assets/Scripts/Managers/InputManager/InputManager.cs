﻿using Assets.Scripts.Managers;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Utils
{
    public class InputManager : MonoBehaviour
    {
        #region Static fields

        private static InputManager _instance;

        public static InputManager Instance
        {
            get { return _instance ?? (_instance = FindObjectOfType<InputManager>()); }
        }

        #endregion

        private readonly List<ContextualAction> _actions = new List<ContextualAction>();
        private readonly List<ContextualAction> _addQueue = new List<ContextualAction>();
        private readonly List<ContextualAction> _removeQueue = new List<ContextualAction>(); 
        public List<X360GamePad> X360GamePads = new List<X360GamePad>();

        void Awake()
        {
            DontDestroyOnLoad(gameObject);
            var i = 1;
            foreach (var joystickName in Input.GetJoystickNames())
            {
                if(!X360GamePads.Exists(pad => pad.Name == joystickName))
                {
                    X360GamePads.Add(new X360GamePad(joystickName, i));
                    Debug.Log("created x360 gamepad `" + joystickName + "' #" + i);
                    ++i;
                }
                
            }
        }

        private void Update()
        {
            foreach (var action in _removeQueue)
            {
                print("removing action : " + (_actions.Remove(action) ? "succed" : "failed"));
            }
            _removeQueue.Clear();

            foreach (var action in _actions.Where(action => action.IsActive()))
            {
                action.Fire();
            }

            foreach (var action in _addQueue)
            {
                _actions.Add(action);
            }
            _addQueue.Clear();
        }

        public void Add(ContextualAction action)
        {
            _addQueue.Add(action);
        }

        public void Remove(ContextualAction action)
        {
            //print("removing " + action.ToString());
            _removeQueue.Add(action);
        }

        public void reset()
        {
            _instance = null;
        }
    }
}