﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Entities;

public class HitResolverPlayer : HitResolver {
    public override void Resolve(Vector2 direction, float force, Player p)
    {
        GetComponent<Player>().SetStunned();
        //GetComponent<Player>().vitesse += direction * force;
        gameObject.GetComponent<Rigidbody2D>().velocity += direction * force * 0.1F;
        //gameObject.GetComponent<Rigidbody2D>().AddForce(direction*force*100,ForceMode2D.Impulse);
    }
}
