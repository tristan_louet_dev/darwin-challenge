﻿using UnityEngine;

namespace Audio {
    public class AudioSourceWrapper : MonoBehaviour {
        public AudioSource EngineAudioSource { get; private set; }

        private AudioData _audioData;
        public AudioData CurrentAudioData {
            get { return _audioData; }
            set {
                _audioData = value;
                _audioData.ConfigureAudioSource(EngineAudioSource);
            }
        }

        private bool _hasStarted;
        private bool _isStoppingFadingOut;
        private bool _fadeIn;
        private bool _fadeOut;
        private float _fadingStartTime;

#if UNITY_EDITOR
        public bool logDebugInfo = false;
#endif

        public delegate void EndOfSoundHandler(AudioSourceWrapper pSrc);
        public event EndOfSoundHandler OnEndOfSound;

        void Awake() {
            _hasStarted = false;
            _isStoppingFadingOut = false;
            EngineAudioSource = gameObject.AddComponent<AudioSource>();
            EngineAudioSource.playOnAwake = false;
            _fadingStartTime = Time.time;
        }

        void Update() {
#if UNITY_EDITOR
            if (logDebugInfo)
            {
                Debug.Log("CurrentAudioData = " + CurrentAudioData);
                Debug.Log("EngineAudioSource = " + EngineAudioSource);
                Debug.Log("Is engine audio source playing ? " + EngineAudioSource.isPlaying);
                Debug.Log("_hasStarted = " + _hasStarted);
                Debug.Log("_isStoppingFadingOut = " + _isStoppingFadingOut);
                Debug.Log("_fadeIn = " + _fadeIn);
                Debug.Log("_fadeOut = " + _fadeOut);
                logDebugInfo = false;
            }
#endif

            if (_audioData == null || !_hasStarted) {
                return;
            }
            
            //Volume + FadeIn FadeOut
            var volume = AudioManager.Instance.ActualChannelsVolume[_audioData.m_channel];
            if (_fadeIn) {
                EngineAudioSource.volume = Mathf.Lerp(0f, volume, (Time.time - _fadingStartTime) / AudioManager.Instance.m_fadeInAndOutSpeed);
                _fadeIn = EngineAudioSource.volume < volume;
            } else if (_isStoppingFadingOut) {
                if (EngineAudioSource.volume > 0f)
                {
                    EngineAudioSource.volume = Mathf.Lerp(volume, 0, (Time.time - _fadingStartTime) / AudioManager.Instance.m_fadeInAndOutSpeed);
                } else {
                    FireEndOfSoundEvent();
                }
            } else {
                EngineAudioSource.volume = volume * _audioData.m_volume;
            }

            if (!_audioData.m_loop && _fadeOut &&
                (EngineAudioSource.time + AudioManager.Instance.m_fadeInAndOutSpeed >= EngineAudioSource.clip.length))
            {
                _isStoppingFadingOut = true;
                _fadingStartTime = Time.time;
            }

            if (!_audioData.m_loop && !_fadeOut && !EngineAudioSource.isPlaying) {
                FireEndOfSoundEvent();
            }
            //Mutable settings of the AudioData
            EngineAudioSource.pitch = CurrentAudioData.m_pitch;
        }

        public void Play(bool pFadeIn, bool pFadeOut) {
            EngineAudioSource.Play();
            _hasStarted = true;
            _fadeIn = pFadeIn;
            if (pFadeIn) {
                EngineAudioSource.volume = 0f;
            }
            _fadeOut = pFadeOut;
        }

        public void Stop() {
            EngineAudioSource.Stop();
            FireEndOfSoundEvent();
        }

        private void FireEndOfSoundEvent() {
            if (OnEndOfSound != null) {
                OnEndOfSound(this);
            }
            _hasStarted = false;
        }
    }
}
