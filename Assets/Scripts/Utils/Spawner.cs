﻿using Assets.Scripts.Entities;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public class Spawner : MonoBehaviour
    {
        [SerializeField] private Team _team = Team.None;
        public Team Team { get { return _team;} }

        void Awake()
        {
            SpawnerManager.Instance.Register(this);
        }
    }
}
