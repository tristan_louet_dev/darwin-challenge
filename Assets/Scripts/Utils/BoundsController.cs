﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Managers;
using System.Collections.Generic;
using Assets.Scripts.Entities;

public class BoundsController : MonoBehaviour {

    public GameObject UpLeft;
    public GameObject DownRight;

    public float PushDistance = 3F;

    private List<GameObject> _playersGO;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        foreach (HumanPLayer hp in HumanPlayersManager.Instance.HumanPLayers)
        {
            GameObject pgo = hp.Player.gameObject;
            if (pgo.transform.position.x < UpLeft.transform.position.x)
            {
                pgo.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                pgo.transform.position += new Vector3(PushDistance, 0, 0);
            }
            if (pgo.transform.position.y > UpLeft.transform.position.y)
            {
                pgo.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                pgo.transform.position += new Vector3(0, -PushDistance, 0);
            }
            if (pgo.transform.position.x > DownRight.transform.position.x)
            {
                pgo.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                pgo.transform.position += new Vector3(-PushDistance, 0, 0);
            }
            if (pgo.transform.position.y < DownRight.transform.position.y)
            {
                pgo.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                pgo.transform.position += new Vector3(0, PushDistance, 0);
            }
        }
        foreach (Ball ball in BallsManager.Instance.Balls)
        {
            if (ball.transform.position.x < UpLeft.transform.position.x)
            {
                ball.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                ball.transform.position += new Vector3(PushDistance, 0, 0);
            }
            if (ball.transform.position.y > UpLeft.transform.position.y)
            {
                ball.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                ball.transform.position += new Vector3(0, -PushDistance, 0);
            }
            if (ball.transform.position.x > DownRight.transform.position.x)
            {
                ball.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                ball.transform.position += new Vector3(-PushDistance, 0, 0);
            }
            if (ball.transform.position.y < DownRight.transform.position.y)
            {
                ball.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                ball.transform.position += new Vector3(0, PushDistance, 0);
            }
        }
	}
}
