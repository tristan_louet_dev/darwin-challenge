﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    class SpriteCollectionManager : MonoBehaviour
    {
        public Sprite[] sprites = null;

        public bool selectRandomSpriteAtStart = false;

        [SerializeField]
        private SpriteRenderer _spriteRenderer;

        private int _currentSpriteIndex = 0;

        void Start()
        {
            if (_spriteRenderer == null)
            {
                _spriteRenderer = GetComponent<SpriteRenderer>();
            }
            if (selectRandomSpriteAtStart)
            {
                SetSpriteInCollection();
            }
        }

        public Sprite GetCurrentSprite()
        {
            return sprites[_currentSpriteIndex];
        }

        public int getSpritesNumber()
        {
            return sprites.Length;
        }

        // set a random sprite if spriteIndex is negative
        public void SetSpriteInCollection(int spriteIndex = -1)
        {
            _currentSpriteIndex = spriteIndex < 0 ? Random.Range(0, (sprites.Length)) : spriteIndex;
            _spriteRenderer.sprite = sprites[_currentSpriteIndex];
        }
    }
}
