﻿using Assets.Scripts.Managers.Events;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public class GameStarter : MonoBehaviour 
    {
        void Start()
        {
            EventManager.OnGameStarting.Fire();
        }
    }
}
