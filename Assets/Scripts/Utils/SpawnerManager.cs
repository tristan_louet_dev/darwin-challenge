﻿using System.Collections.Generic;
using System.Xml;
using Assets.Scripts.Entities;
using Assets.Scripts.Managers;
using Assets.Scripts.Managers.Events;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public class SpawnerManager : Singleton<SpawnerManager>
    {
        private Dictionary<Team, SpawnerWrapper> _teamSpawners;

        private struct SpawnerWrapper
        {
            public List<Spawner> Spawners;
            public int Index;
        }

        public override void Awake()
        {
            base.Awake();
            _teamSpawners = new Dictionary<Team, SpawnerWrapper>();
            foreach (var team in TeamExtensions.Values())
            {
                SpawnerWrapper s = new SpawnerWrapper
                {
                    Spawners = new List<Spawner>(),
                    Index = 0
                };
                _teamSpawners.Add(team, s);
                Debug.Log("List of spawners for team " + team + " created");
            }
        }

        public void Register(Spawner s)
        {
            _teamSpawners[s.Team].Spawners.Add(s);
            Debug.Log("Spawner " + s + " for team " + s.Team + " registered. Count is now " +
                      _teamSpawners[s.Team].Spawners.Count);
        }

        public void Spawn(Player p)
        {
            var wrapper = _teamSpawners[p.Team];
            p.transform.position = wrapper.Spawners[wrapper.Index].transform.position;
            p.ballCatcher.gameObject.transform.position = new Vector2(0.0f, 0.0f);
            wrapper.Index = (wrapper.Index + 1) % wrapper.Spawners.Count;
        }
    }
}
