﻿using System;
using UnityEngine;

namespace Assets.Scripts.Utils {
    public static class Vector2Extensions {
        public static bool Any(this Vector2 v, Func<float, bool> compareFunc)
        {
            return compareFunc(v.x) || compareFunc(v.y);
        }
    }
}
