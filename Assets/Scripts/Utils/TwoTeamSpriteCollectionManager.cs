﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    class TwoTeamSpriteCollectionManager : MonoBehaviour
    {
        public Sprite[] blueTeamSprites;
        public Sprite[] yellowTeamSprites;

        public bool selectRandomSpriteAtStart = false;
        public bool isBlueTeam = true;

        [SerializeField]
        private SpriteRenderer _spriteRenderer;

        private int _currentSpriteIndex = 0;
        private bool _isCurrentTeamBlue = true;

        void Start()
        {
            if (_spriteRenderer == null)
            {
                _spriteRenderer = GetComponent<SpriteRenderer>();
            }
            if(selectRandomSpriteAtStart)
            {
                SetTeamSpriteInCollection(this.isBlueTeam);
            }
            if (blueTeamSprites == null)
            {
                blueTeamSprites = new Sprite[0];
            }
            if (yellowTeamSprites == null)
            {
                yellowTeamSprites = new Sprite[0];
            }
        }

        public Sprite GetCurrentTeamSprite()
        {
            return _isCurrentTeamBlue ? blueTeamSprites[_currentSpriteIndex] : yellowTeamSprites[_currentSpriteIndex];
        }

        public int getTeamSpritesNumber(bool isBlueTeam)
        {
            return isBlueTeam ? blueTeamSprites.Length : yellowTeamSprites.Length;
        }

        // set a random sprite if spriteIndex is negative
        public void SetTeamSpriteInCollection(bool isBlueTeam, int spriteIndex = -1)
        {
            _isCurrentTeamBlue = isBlueTeam;
            _currentSpriteIndex = spriteIndex < 0 ? Random.Range(0, (_isCurrentTeamBlue ? blueTeamSprites.Length : yellowTeamSprites.Length)) : spriteIndex;
            _spriteRenderer.sprite = _isCurrentTeamBlue ? blueTeamSprites[_currentSpriteIndex] : yellowTeamSprites[_currentSpriteIndex];
        }
    }
}
