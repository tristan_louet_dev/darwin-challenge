﻿using UnityEngine;
using System.Collections.Generic;

public class BodyPossibilities : MonoBehaviour 
{
	public enum Combinations {JelBeaPol, JelBeaKan, JelEagPol, JelEagKan, ChaBeaPol, ChaBeaKan, ChaEagPol, ChaEagKan};

	public Dictionary<Combinations, string> names;

	// Use this for initialization
	void Awake () {
		names = new Dictionary<Combinations, string> {
			{Combinations.JelBeaPol, "JelBeaPol"},
			{Combinations.JelBeaKan, "JelBeaKan"},
			{Combinations.JelEagPol, "JelEagPol"},
			{Combinations.JelEagKan, "JelEagKan"},
			{Combinations.ChaBeaPol, "ChaBeaPol"},
			{Combinations.ChaBeaKan, "ChaBeaKan"},
			{Combinations.ChaEagPol, "ChaEagPol"},
			{Combinations.ChaEagKan, "ChaEagKan"}
		};
	}
}
