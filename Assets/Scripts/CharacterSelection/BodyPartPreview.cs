﻿using UnityEngine;
using System.Collections;

public class BodyPartPreview : MonoBehaviour 
{
    public Sprite bodyPartSpriteBlue;
    public Sprite bodyPartSpriteYellow;
    public Sprite capacityTextSprite;
    public GameObject gameBodyPrefab;

    public int speedBonus = 0;
    public int jumpBonus = 0;
    public int gripBonus = 0;
    public int resilienceBonus = 0;
    public int handlingBonus = 0;

    public Sprite getBodyPartSprite(bool blueTeam)
    {
        return blueTeam ? bodyPartSpriteBlue : bodyPartSpriteYellow;
    }
}
