﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.CharacterSelection
{
    class SpriteRandomizer : MonoBehaviour
    {
        public Sprite[] sprites;
        public float changeDelay = 1.0f;
        public float ChangeDelayOffset = 0.2f;

        private float _timeToChange = 0.0f;

        void Start ()
        {
            sprites = sprites ?? new Sprite[0];

            if (GetComponent<SpriteRenderer>())
            {
                GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Length)];
                _timeToChange = Time.time + changeDelay;
            }
        }

        void Update ()
        {
            if(Time.time >= _timeToChange)
            {
                if(GetComponent<SpriteRenderer>())
                {
                    GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Length)];
                    _timeToChange = Time.time + changeDelay + Random.Range(-ChangeDelayOffset, changeDelay);
                }
                
            }
        }
    }
}
