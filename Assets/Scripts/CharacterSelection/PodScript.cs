﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Managers;
using Assets.Scripts.CharacterSelection;
using Audio;

// manage pod feedbacks

public class PodScript : MonoBehaviour 
{
    public GameObject zoneUp;
    public GameObject zoneMid;
    public GameObject zoneDown;
    public GameObject door;
    public GameObject innerLight;
    public GameObject backPod;
    public GameObject helpText;
    public GameObject screenDisplayWait;
    public GameObject screenDisplayModify;
    public GameObject scientist;

    public GameObject lightSpeed;
    public GameObject lightJump;
    public GameObject lightGrip;
    public GameObject lightResilience;
    public GameObject lightHandling;

    public Sprite[] lightSprites;

    public float closingDoorPitch = 2.5f;

    public GameObject changeZoneAudio;
    public float upChangeZonePitch = 0.42f;
    public float downChangeZonePitch = 0.4f;

    public GameObject randomizeAudio;
    public GameObject playerValidationAudio;

    public bool blueTeam = true;

    public enum SelectionState {wait = 0, modify = 1, ready = 2};
    public SelectionState selectionState = SelectionState.wait;

    public enum BodyPart {head = 0, chest = 1, legs = 2};
    private BodyPart _currentBodyPart = BodyPart.head;

    private ZoneSelector[] _bodyParts;
    private Animator _doorAnimator;
    private AudioData _doorAudioData;
    private AudioData _changeZoneAudioData;

    private bool _hasAlreadyOpenDoor = false;

    // Use this for initialization
	void Start ()
    {
        _bodyParts = new ZoneSelector[3];
        _bodyParts[0] = (ZoneSelector)zoneUp.GetComponent<ZoneSelector>();
        _bodyParts[1] = (ZoneSelector)zoneMid.GetComponent<ZoneSelector>();
        _bodyParts[2] = (ZoneSelector)zoneDown.GetComponent<ZoneSelector>();

        foreach(ZoneSelector part in _bodyParts)
        {
            part.setCurrentPart(false);
            part.setTeam(blueTeam);
        }

        _doorAnimator = door.GetComponent<Animator>();

        helpText.GetComponent<textInformationManager>().setWaitState();
        backPod.GetComponent<TwoTeamSpriteManager>().SetTeamSprite(blueTeam);
        innerLight.GetComponent<TwoTeamSpriteManager>().SetTeamSprite(blueTeam);
        innerLight.SetActive(false);
        screenDisplayWait.SetActive(true);
        screenDisplayModify.SetActive(false);

        _doorAudioData = door.GetComponent<AudioData>();
        _changeZoneAudioData = changeZoneAudio.GetComponent<AudioData>();
	}

    public void changeZone(bool goUp)
    {
        _bodyParts[(int)_currentBodyPart].setCurrentPart(false);
        if (goUp)
        {
            int nextZone = ((int)_currentBodyPart - 1) % 3;
            _currentBodyPart = (BodyPart)(nextZone >= 0 ? nextZone : 2);
            _changeZoneAudioData.m_pitch = upChangeZonePitch;
            AudioManager.Instance.Play(_changeZoneAudioData);
        }
        else
        {
            _currentBodyPart = (BodyPart)(((int)_currentBodyPart + 1) % 3);
            _changeZoneAudioData.m_pitch = downChangeZonePitch;
            AudioManager.Instance.Play(_changeZoneAudioData);
        }
        _bodyParts[(int)_currentBodyPart].setCurrentPart(true);
        scientist.GetComponent<Animator>().SetTrigger("look");
    }

    public void changeBodyPart(bool goLeft)
    {
        _bodyParts[(int)_currentBodyPart].changeBodyPart(goLeft);
        displayCaracteristics();
        scientist.GetComponent<Animator>().SetTrigger("look");
    }

    public GameObject getBodyPartGamePrefab(BodyPart bodyPart)
    {
        return _bodyParts[(int)bodyPart].getCurrentGameBodyPrefab();
    }

    public void RandomCharacter()
    {
        if(selectionState == SelectionState.modify)
        {
            _doorAnimator.SetTrigger("randomizing");
            AudioManager.Instance.Play(randomizeAudio.GetComponent<AudioData>());
            StartCoroutine(waitForRandom());
        }
    }

    IEnumerator waitForRandom()
    {
        yield return new WaitForSeconds(0.35f);
        computeRandom();
    }

    public void computeRandom()
    {
        int forceChangingPart = Random.Range(0, 3);
        for (int i = 0; i < _bodyParts.Length; i++)
        {
            if (i == forceChangingPart)
            {
                _bodyParts[i].SetRandomBodyPart(_bodyParts[i].getCurrentBodyPart());
            }
            else
            {
                _bodyParts[i].SetRandomBodyPart();
            }
        }
        displayCaracteristics();
        scientist.GetComponent<Animator>().SetTrigger("write");
    }

    public void OpenDoor()
    {
        if (selectionState == SelectionState.wait)
        {
            if(!_hasAlreadyOpenDoor)
            {
                _hasAlreadyOpenDoor = true;
                computeRandom();
            }
            else
            {
                scientist.GetComponent<Animator>().SetTrigger("write");
            }

            _doorAnimator.SetTrigger("openning");
            _doorAudioData.m_pitch = 1.0f;
            AudioManager.Instance.Play(_doorAudioData);

            selectionState = SelectionState.modify;
            helpText.GetComponent<textInformationManager>().setModifyState();
            _bodyParts[(int)_currentBodyPart].setCurrentPart(true);
            screenDisplayWait.SetActive(false);
            screenDisplayModify.SetActive(true);
            displayCaracteristics();
        }
    }

    public void CloseDoor()
    {
        if (selectionState == SelectionState.modify)
        {
            _doorAnimator.SetTrigger("closing");
            _doorAudioData.m_pitch = closingDoorPitch;
            AudioManager.Instance.Play(_doorAudioData);
            
            selectionState = SelectionState.wait;
            helpText.GetComponent<textInformationManager>().setWaitState();
            _bodyParts[(int)_currentBodyPart].setCurrentPart(false);
            screenDisplayWait.SetActive(true);
            screenDisplayModify.SetActive(false);
            scientist.GetComponent<Animator>().SetTrigger("write");
        }
    }

    public void SetPlayerReady()
    {
        selectionState = SelectionState.ready;
        helpText.GetComponent<textInformationManager>().setReadyState();
        CharacterSelectionManager.Instance.checkReadyPlayers();
        _bodyParts[(int)_currentBodyPart].setCurrentPart(false);
        innerLight.SetActive(true);
        AudioManager.Instance.Play(playerValidationAudio.GetComponent<AudioData>());
        scientist.GetComponent<Animator>().SetTrigger("write");
    }

    public void SetPlayerNotReady()
    {
        selectionState = SelectionState.modify;
        helpText.GetComponent<textInformationManager>().setModifyState();
        CharacterSelectionManager.Instance.checkReadyPlayers();
        _bodyParts[(int)_currentBodyPart].setCurrentPart(true);
        innerLight.SetActive(false);
    }

    private void displayCaracteristics()
    {
        // caracteristics are, in this order : speed, jump, grip, resilience and handling
        int[] caracteristics = new int[5];

        for (int i = 0; i < caracteristics.Length; i++)
        {
            caracteristics[i] = 3;
        }

        foreach(ZoneSelector zone in _bodyParts)
        {
            caracteristics[0] += zone.getCurrentBodyPartPreview().speedBonus;
            caracteristics[1] += zone.getCurrentBodyPartPreview().jumpBonus;
            caracteristics[2] += zone.getCurrentBodyPartPreview().gripBonus;
            caracteristics[3] += zone.getCurrentBodyPartPreview().resilienceBonus;
            caracteristics[4] += zone.getCurrentBodyPartPreview().handlingBonus;
        }

        setLightsSprite(lightSpeed.GetComponent<SpriteRenderer>(), caracteristics[0]);
        setLightsSprite(lightJump.GetComponent<SpriteRenderer>(), caracteristics[1]);
        setLightsSprite(lightGrip.GetComponent<SpriteRenderer>(), caracteristics[2]);
        setLightsSprite(lightResilience.GetComponent<SpriteRenderer>(), caracteristics[3]);
        setLightsSprite(lightHandling.GetComponent<SpriteRenderer>(), caracteristics[4]);
    }

    private void setLightsSprite(SpriteRenderer lightRenderer, int lightsOn)
    {
        if(lightsOn <= 1)
        {
            lightRenderer.sprite = lightSprites[0];
        }
        else if (lightsOn >= 5)
        {
            lightRenderer.sprite = lightSprites[4];
        }
        else
        {
            lightRenderer.sprite = lightSprites[lightsOn - 1];
        }
    }
}
