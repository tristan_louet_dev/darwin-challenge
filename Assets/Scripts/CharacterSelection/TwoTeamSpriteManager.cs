﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.CharacterSelection
{
    class TwoTeamSpriteManager : MonoBehaviour
    {
        public Sprite blueTeamSprite;
        public Sprite yellowTeamSprite;

        private SpriteRenderer _spriteRenderer;

        void Start ()
        {
            blueTeamSprite = blueTeamSprite ?? new Sprite();
            yellowTeamSprite = yellowTeamSprite ?? new Sprite();
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        public Sprite GetTeamSprite(bool blueTeam)
        {
            return blueTeam ? blueTeamSprite : yellowTeamSprite;
        }

        public void SetTeamSprite(bool blueTeam)
        {
            _spriteRenderer.sprite = blueTeam ? blueTeamSprite : yellowTeamSprite;
        }
    }
}
