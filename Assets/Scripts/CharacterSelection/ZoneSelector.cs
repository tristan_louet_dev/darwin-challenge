﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Audio;

public class ZoneSelector : MonoBehaviour 
{
    public SpriteRenderer bodyPartRenderer;
    public GameObject[] bodyParts;

    public GameObject arrowLeft;
    public GameObject arrowRight;
    public GameObject capacityTextDisplayer;

    public float highlightDelay = 0.5f;         // time to wait when an arrow is on selected stay before going back to highlight
    public bool blueTeam = true;
    
    //for later
    //public SpriteRenderer CapacityTextRenderer;

    private List<BodyPartPreview> _bodyPartPreviews;
    private int _currentBodyPart = 0;
    private ArrowSelection _arrowRight;
    private ArrowSelection _arrowLeft;
    private SpriteRenderer _capacityTextSpriteRenderer;

    private float _backToHighlightTime = 0.0f;  // time when arrows go back to higlight
    private bool _isArrowOnSelect = false;

	// Use this for initialization
	void Start ()
    {
        // fill bodyPartPreviews
        _bodyPartPreviews = new List<BodyPartPreview>();
	    foreach(GameObject bodyPart in bodyParts)
        {
            _bodyPartPreviews.Add((BodyPartPreview)bodyPart.GetComponent<BodyPartPreview>());
        }

        bodyPartRenderer.sprite = _bodyPartPreviews[0].getBodyPartSprite(blueTeam);

        if(_arrowLeft == null || _arrowRight == null)
        {
            initArrowSelectors();
        }

        _capacityTextSpriteRenderer = capacityTextDisplayer.GetComponent<SpriteRenderer>();
	}

    private void initArrowSelectors()
    {
        _arrowLeft = (ArrowSelection)arrowLeft.GetComponent<ArrowSelection>();
        _arrowRight = (ArrowSelection)arrowRight.GetComponent<ArrowSelection>();
    }

    void Update()
    {
        if (_isArrowOnSelect && Time.time >= _backToHighlightTime)
        {
            _arrowLeft.setState(ArrowSelection.ArrowState.highlight);
            _arrowRight.setState(ArrowSelection.ArrowState.highlight);
            _isArrowOnSelect = false;
        }
    }

    public void changeBodyPart(bool goLeft)
    {
        if (goLeft)
        {
            int nextBodyPart = (_currentBodyPart - 1) % _bodyPartPreviews.Count;
            _currentBodyPart = (nextBodyPart >= 0) ? nextBodyPart : (_bodyPartPreviews.Count - 1);
            _arrowLeft.setState(ArrowSelection.ArrowState.select);
            _arrowRight.setState(ArrowSelection.ArrowState.neutral);
            AudioManager.Instance.Play(_arrowRight.GetComponent<AudioData>());
        }
        else
        {
            _currentBodyPart = (_currentBodyPart + 1) % _bodyPartPreviews.Count;
            _arrowRight.setState(ArrowSelection.ArrowState.select);
            _arrowLeft.setState(ArrowSelection.ArrowState.neutral);
            AudioManager.Instance.Play(_arrowLeft.GetComponent<AudioData>());
        }

        updateSprites();
        _backToHighlightTime = Time.time + highlightDelay;
        _isArrowOnSelect = true;
    }

    public void setCurrentPart(bool isCurrentPart)
    {
        if(_arrowRight == null ||_arrowRight == null)
        {
            initArrowSelectors();
        }

        if(isCurrentPart)
        {
            _arrowRight.setState(ArrowSelection.ArrowState.highlight);
            _arrowLeft.setState(ArrowSelection.ArrowState.highlight);
        }
        else
        {
            _arrowRight.setState(ArrowSelection.ArrowState.neutral);
            _arrowLeft.setState(ArrowSelection.ArrowState.neutral);
        }
        _isArrowOnSelect = false;
    }

    public GameObject getCurrentGameBodyPrefab()
    {
        return _bodyPartPreviews[_currentBodyPart].gameBodyPrefab;
    }

    public int getCurrentBodyPart()
    {
        return _currentBodyPart;
    }

    public BodyPartPreview getCurrentBodyPartPreview()
    {
        return _bodyPartPreviews[_currentBodyPart];
    }

    public void SetRandomBodyPart()
    {
        _currentBodyPart = Random.Range(0, _bodyPartPreviews.Count);
        updateSprites();
    }

    // set a random bodypart exept one (specified with the int)
    public void SetRandomBodyPart(int exceptPart)
    {
        if(exceptPart < _bodyPartPreviews.Count)
        {
            do
            {
                _currentBodyPart = Random.Range(0, _bodyPartPreviews.Count);
            } while (_currentBodyPart == exceptPart);

            updateSprites();
        }
        else
        {
            SetRandomBodyPart();
        }
    }
    
    public void setTeam(bool isBlueTeam)
    {
        blueTeam = isBlueTeam;
        updateSprites();
    }

    private void updateSprites()
    {
        bodyPartRenderer.sprite = _bodyPartPreviews[_currentBodyPart].getBodyPartSprite(blueTeam);
        _capacityTextSpriteRenderer.sprite = _bodyPartPreviews[_currentBodyPart].capacityTextSprite;
    }
}
