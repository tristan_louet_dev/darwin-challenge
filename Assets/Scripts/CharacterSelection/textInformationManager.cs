﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.CharacterSelection
{
    class textInformationManager : MonoBehaviour
    {
        public Sprite randomTextSprite;
        public Sprite confirmTextSprite;
        public Sprite backTextSprite;

        public float transitionDelay = 1.0f; // delay transition between random and confirm hints

        private bool _isPodOnModify;

        private SpriteRenderer _spriteRenderer;

        private float _transitionTime; // time to change sprite

        void Start ()
        {
            randomTextSprite = randomTextSprite ?? new Sprite();
            confirmTextSprite = confirmTextSprite ?? new Sprite();
            backTextSprite = backTextSprite ?? new Sprite();
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        void Update()
        {
            if(_isPodOnModify && (Time.time >= _transitionTime))
            {
                _spriteRenderer.sprite = _spriteRenderer.sprite == randomTextSprite ? confirmTextSprite : randomTextSprite;
                _transitionTime = Time.time + transitionDelay;
            }
        }

        public void setWaitState()
        {
            this.gameObject.SetActive(false);
        }

        public void setModifyState()
        {
            this.gameObject.SetActive(true);
            _spriteRenderer.sprite = confirmTextSprite;
            _transitionTime = Time.time + transitionDelay;
            _isPodOnModify = true;
        }

        public void setReadyState()
        {
            this.gameObject.SetActive(true);
            _spriteRenderer.sprite = backTextSprite;
            _transitionTime = 0;
            _isPodOnModify = false;
        }
    }
}
