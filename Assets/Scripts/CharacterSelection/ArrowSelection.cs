﻿using UnityEngine;
using System.Collections;

public class ArrowSelection : MonoBehaviour
{
    public Sprite highlihtSprite;
    public Sprite neutralSprite;
    public Sprite selectSprite;

    public enum ArrowState {neutral = 0, highlight = 1, select = 2};

    private SpriteRenderer _spriteRender;

    void Start ()
    {
        if(_spriteRender == null)
        {
            initSpriteRenderer();
        }
    }

    // in case the the script starts too late and _spriteRenderer is not initialize
    private void initSpriteRenderer()
    {
        _spriteRender = GetComponent<SpriteRenderer>();
    }

    public void setState(ArrowState arrowState)
    {
        if (_spriteRender == null)
        {
            initSpriteRenderer();
        }

        switch (arrowState)
        {
            case ArrowState.neutral :
                _spriteRender.sprite = neutralSprite;
                break;
                
            case ArrowState.highlight :
                _spriteRender.sprite = highlihtSprite;
                break;

            case ArrowState.select :
                _spriteRender.sprite = selectSprite;
                break;

            default :
                break;
        }
    }

}
